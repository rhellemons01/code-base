import { WebApplicationPage } from './app.po';

describe('web-application App', () => {
  let page: WebApplicationPage;

  beforeEach(() => {
    page = new WebApplicationPage();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
