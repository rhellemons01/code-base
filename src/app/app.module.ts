import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';

import {ToastModule, ToastOptions} from 'ng2-toastr';
import {CustomToastConfigOption} from './_shared/config/toast.config';
import {AngularFireModule} from 'angularfire2';
import {firebaseConfig} from './_shared/config/firebase.config';
import {AngularFireAuthModule} from 'angularfire2/auth';
import {AngularFireDatabaseModule} from 'angularfire2/database';
import {AppRoutingModule} from "./app.routes";
import {AppComponent} from "./app.component";
import {RouterModule} from "@angular/router";
import {FirebaseAuthenticationService} from "./_shared/services/firebase-db/authentication.service";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {AuthStateGuard} from "./_shared/guards/authState.guard";
import {BsDropdownModule} from 'ngx-bootstrap/dropdown';
import {TypeaheadModule} from 'ngx-bootstrap/typeahead';
import {PageTitleService} from "./_shared/services/page-title.service";
import {ACCESS_TOKEN, ENDPOINT, linkResolver} from "./_shared/config/prismic.config";
import {DocumentsModule} from "./public/documents/documents.module";
import {PrismicService} from "./_shared/services/prismic.service";
import {ProfileModule} from "./public/profile/profile.module";
import {CategoryModule} from "./public/category/category.module";
import {AlphabetService} from "./_shared/services/alphabet.service";
import {ResultsModule} from "./public/results/results.module";
import {FirebaseCategoryService} from "./_shared/services/firebase-db/categories.service";
import {FirebaseCompanyService} from "./_shared/services/firebase-db/companies.service";
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import {FirebaseProfileService} from "./_shared/services/firebase-db/profile.service";


/**
 *  Global Services
 */
const APP_SERVICES = [
  AlphabetService,
  FirebaseAuthenticationService,
  FirebaseCategoryService,
  FirebaseCompanyService,
  FirebaseProfileService,
  PageTitleService,
  PrismicService
];

const APP_GUARDS = [
  AuthStateGuard
];




/**
 * Config Toastr
 */

const TOAST_CONFIG = [
  {provide: ToastOptions, useClass: CustomToastConfigOption},
];


/**
 * Config Prismic
 */

const PRISMIC_CONFIG = [
  {provide: 'PrismicEndpoint', useValue: ENDPOINT},
  {provide: 'PrismicAccessToken', useValue: ACCESS_TOKEN},
  {provide: 'LinkResolver', useValue: linkResolver}
]



@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule,
    BsDropdownModule.forRoot(),
    TypeaheadModule.forRoot(),
    CategoryModule,
    DocumentsModule,
    FormsModule,
    HttpModule,
    InfiniteScrollModule,
    ProfileModule,
    ToastModule.forRoot(),
    ResultsModule,
    RouterModule
  ],
  providers: [
    APP_GUARDS,
    APP_SERVICES,
    PRISMIC_CONFIG,
    TOAST_CONFIG,],
  bootstrap: [AppComponent]
})
export class AppModule {
}
