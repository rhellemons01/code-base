import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import {RouterModule} from "@angular/router";
import {FormsModule} from "@angular/forms";
import {ImageUploadModule} from "../../_partials/image-upload/image-upload.module";



export const routes = [
  {path: '', data: { title: 'Dashboard' }, component: DashboardComponent,  pathMatch: 'full'}
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ImageUploadModule,
    RouterModule.forChild(routes),
  ],
  declarations: [DashboardComponent],
  exports:[DashboardComponent]
})
export class DashboardModule { }
