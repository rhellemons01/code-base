import {Component, OnInit} from '@angular/core';
import {PageTitleService} from "../../_shared/services/page-title.service";
import {FirebaseAuthenticationService} from "../../_shared/services/firebase-db/authentication.service";
import {Subscription} from "rxjs/Subscription";

@Component({
  selector: 'gb-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  displayname: string;

  subAuth: Subscription;
  setDisplayName : boolean = false

  constructor(private titleService: PageTitleService,
              private authService: FirebaseAuthenticationService) {
    this.titleService.insertTitle('Dashboard');
  }

  ngOnInit() {
    this.loadDisplayname();
  }

  loadDisplayname() {
    this.subAuth = this.authService.getCurrentUser()
      .subscribe(user => {
        const check = user.displayName;
        if(check) {
          this.displayname = user.displayName
        }else {
          this.setDisplayName = true
        }
      })
  }

  updateDisplayname() {
    const name = this.displayname
    this.authService.updateDisplayname(name)
      .subscribe(succes => console.log('success', name))
  }


  ngOnDestroy() {
    this.subAuth.unsubscribe();
  }

}
