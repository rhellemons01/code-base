import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AdminComponent} from "./admin.component";



const adminRoutes: Routes = [

  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full'
  },
  {
    path: '',
    component: AdminComponent,
    children: [
      {
        path: '',
        children: [
          { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule'},
          {
            path: 'categories',
            data: {title: 'Category Management'},
            loadChildren: './categories/categories.module#AdminCategoriesModule'
          },
          {
            path: 'profiles',
            data: {title: 'Profile Management'},
            loadChildren: './profile/profile.module#AdminProfileModule'
          },
          {
            path: 'companies',
            data: {title: 'Company Management'},
            loadChildren: './company-profile/company-profile.module#CompanyProfileModule'
          },
        ],
      }
    ]
  },


];

@NgModule({
  imports: [
    RouterModule.forChild(adminRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AdminRoutingModule {
}
