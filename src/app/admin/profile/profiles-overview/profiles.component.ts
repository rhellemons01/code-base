import {Component, OnDestroy, OnInit} from '@angular/core';
import {PageTitleService} from '../../../_shared/services/page-title.service';
import {FormBuilder, FormGroup} from "@angular/forms";
import {Observable} from "rxjs/Observable";
import {SourceProfileInterface} from "../../../_shared/interface/profile/source/source-profile.interface";
import {FirebaseProfileService} from "../../../_shared/services/firebase-db/profile.service";
import {TypeaheadMatch} from "ngx-bootstrap";
import {Router} from "@angular/router";


@Component({
  selector: 'app-categories',
  templateUrl: './profiles.component.html',
  styleUrls: ['./profiles.component.scss']
})
export class ProfilesComponent implements OnInit, OnDestroy {

  controllerProfileForm: FormGroup;

  Alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];

  selectedLetter: string = 'a';
  selectedStatus;

  isSearch: boolean = false

  public asyncSelected: string;
  public typeaheadLoading: boolean = false;
  public typeaheadNoResults: boolean;
  public dataSource: Observable<any>;
  profileList$: Observable<SourceProfileInterface[]>; // Load all Profiles[name + key]


  constructor(private fb: FormBuilder,
              private profileService: FirebaseProfileService,
              private titleService: PageTitleService,
              private router: Router) {
  }

  ngOnInit() {
    this.titleService.insertTitle('Profile Management');
    this.buildControllerForm();
  }


  buildControllerForm() {
    this.controllerProfileForm = this.fb.group({
      searchInput: [''],
      collectionSelect: ['0']
    })

    this.initDataSource();
    this.sortByPublishedStatus()

  }


  toggleLetter(value) {
    this.selectedLetter = value;
    this.isSearch = false;
    this.controllerProfileForm.patchValue({
      levelSelect: '0',
      collectionSelect: '0'
    })
    return 'active-alp'
  }


  sortByPublishedStatus() {
    this.controllerProfileForm.get('collectionSelect').valueChanges
      .subscribe(value => {
        this.selectedStatus = value;
        console.log('Publish click: ', this.selectedStatus, '=', value)
      })
  }


  /**
   * Search function,
   * if string has value execute search
   */
  initDataSource() {
    this.dataSource = Observable
      .create((observer: any) => {
        // Runs on every search
        observer.next(this.asyncSelected);
      })
      .mergeMap((token: string) => this.getProfileData(token))
  }


  /**
   * Return all profiles that match string
   * @param {string} token
   * @returns {Observable<R>}
   */
  getProfileData(token: string): Observable<SourceProfileInterface[]> {
    return this.profileList$ = this.profileService.getProfilesBySearch(token).first()
      .map((list: SourceProfileInterface[]) => list.filter(item => item.isActive == undefined));
  }

  /**
   * Show Icon during search typeahead
   * @param e
   */
  public changeTypeaheadLoading(e: boolean): void {
    this.typeaheadLoading = e;
  }

  /**
   * Show No result if search has noresults
   * @param e
   */
  public changeTypeaheadNoResults(e: boolean): void {
    this.typeaheadNoResults = e;
  }

  /**
   * Typeahead selected Company
   * Set ID and Name
   * @param e
   */
  public typeaheadOnSelect(e: TypeaheadMatch): void {
    const key = e.item.$key
    this.router.navigate(['admin/profiles/manage/', key])
    this.controllerProfileForm.reset()
  }


  ngOnDestroy() {
    this.selectedStatus = '';
  }


}
