import {Component, EventEmitter, Input, OnChanges, OnDestroy, Output, SimpleChanges} from '@angular/core';

import {Subscription} from "rxjs/Subscription";
import {SourceProfileInterface} from "../../../../_shared/interface/profile/source/source-profile.interface";
import {FirebaseProfileService} from "../../../../_shared/services/firebase-db/profile.service";

@Component({
  selector: 'gb-profiles-by-letter',
  templateUrl: './profiles-letter-container.component.html',
  styleUrls: ['./profiles-letter-container.component.scss']
})
export class ProfilesLetterContainerComponent implements OnChanges, OnDestroy {

  /** Backup Data for Filter **/
  sourceProfiles: SourceProfileInterface[] = [];

  /** Data for Template **/
  profiles: SourceProfileInterface[] = [];

  profilesSub: Subscription;
  inputLetter: string


  @Input() letter: string = 'a';
  @Input() sortbypublished: number

  @Output() selectedLetter = new EventEmitter;


  constructor(private profileService: FirebaseProfileService) {
  }


  ngOnChanges(changes: SimpleChanges) {
    const change1 = changes['letter'];
    if (change1) {
      this.loadProfilesByLetter(change1.currentValue)
    }


    const change2 = changes['sortbypublished'];
    if (change2) {
      this.filterByPublished(change2.currentValue)
    }

  }


  /**
   * Check to set Icon class for Published
   * @param value
   * @returns {any}
   */
  getPublishedClass(value) {
    if (value) {
      return 'fa-check-circle-o font-green'
    }
    if (!value) {
      return 'fa-circle-o font-grey'
    }
  }


  /**
   * Get CompanyName By Company ID from Company node
   * @param value
   * @returns {any}
   */
  // ToDo: This function is disabled: maxes out CPU due to Angular Change Detection we got multiple simultaneous requests
  // getCompanyName(id) {
  //   this.companyService.getCompanyByID(id)
  //     .first()
  //     .subscribe(company =>{
  //       return company.CompanyName
  //     })
  //
  //   console.log(id)
  // }


  /**
   * Get Profiles from Firebase that match Letter
   * Filter locally due to Capitol and LowerCase entries
   * @param letter
   */
  loadProfilesByLetter(letter: string) {
    const check = letter;
    this.inputLetter = letter;

    if (!check) {
      this.inputLetter = 'A'
    }


    this.profilesSub = this.profileService.getProfilesByLetter(this.inputLetter.toUpperCase())
      .first()
      .map((list: SourceProfileInterface[]) => list.filter(item => item.isActive == undefined))
      .subscribe(list => {
        this.sourceProfiles = list;
        this.profiles = list;
        console.log(list.length)
      })

    this.selectedLetter.emit(this.inputLetter)
  }


  /**
   *
   * @param Status
   * @returns {SourceProfileInterface[]}
   */
  filterByPublished(Status) {
    console.log('Start Filter on Publication: ', Status)
    const source = this.sourceProfiles;
    if (Status == 0) {
      console.log('All')
      return this.profiles = source;
    }
    if (Status == 1) {
      console.log('Published')
      const listT = source.filter(list => {
        return list.published === true
      })
      console.log('True', listT)
      return this.profiles = listT;
    }
    if (Status == 2) {
      console.log('Not Published')
      const list = source.filter(list => {
        return list.published == undefined
      })
      console.log('False', list)
      return this.profiles = list;
    }
  }


  ngOnDestroy() {
    this.profiles = [];
    this.sourceProfiles = [];
  }

}
