import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {SourceProfileInterface} from "../../../_shared/interface/profile/source/source-profile.interface";
import {FirebaseProfileService} from "../../../_shared/services/firebase-db/profile.service";
import {Observable} from "rxjs/Observable";


@Component({
  selector: 'gb-profiles-per-company',
  templateUrl: './company-profile-overview.component.html',
  styleUrls: ['./company-profile-overview.component.scss']
})
export class CompanyProfileOverviewComponent implements OnChanges {


  profiles$: Observable<SourceProfileInterface[]>

  @Input() CompanyName: string = 'Company Name';
  @Input() CompanyID: number;

  // Close Modal
  @Output() hideCompanyModal = new EventEmitter;
  @Output() routeToProfileKey = new EventEmitter;


  constructor(private profileService: FirebaseProfileService) {
  }


  ngOnChanges(changes: SimpleChanges) {
    const change1 = changes['CompanyID'];
    if (change1) {
      this.getCompanyProfilelist(change1.currentValue)
    }


  }


  /**
   * Check to set Icon class for Published
   * @param value
   * @returns {any}
   */
  getPublishedClass(value) {
    if (value) {
      return 'fa-check-circle-o font-green'
    }
    if (!value) {
      return 'fa-circle-o font-grey'
    }
  }


  /**
   *
   * @param id
   */
  getCompanyProfilelist(id) {
    if (id != null) {
      this.profiles$ = this.profileService.getProfilesByCompany(id)
        .first()
    }
  }


  /**
   * Get Main Category and populate form
   * @param id
   * ToDo: Function drains CPU due to Change detection Angular we make to many requests
   */
  // getMainCategoryByID(id) {
  //   let category = ' N/A '
  //   if (id) {
  //     const cat$ :Subscription = this.profileService.getMainCategoryByID(id)
  //       .first()
  //       .subscribe(cat => { return category = cat.maincategoryName })
  //
  //
  //     return category
  //   }
  // }


  /**
   *
   * @param id
   */
  goToProfile(id) {
    this.routeToProfileKey.emit(id)
  }

  /**
   *
   */
  closeCompanyModal() {
    this.hideCompanyModal.emit(false);
  }

}
