import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {FirebaseAuthenticationService} from "../../../_shared/services/firebase-db/authentication.service";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {Subscription} from "rxjs/Subscription";
import {User} from "firebase/app";
import {FirebaseCategoryService} from "../../../_shared/services/firebase-db/categories.service";
import {Observable} from "rxjs/Observable";
import {CategoryInterface} from "../../../_shared/interface/category/category.interface";
import {CompanyImportInterface} from "../../../_shared/interface/company/company.interface";
import {FirebaseCompanyService} from "../../../_shared/services/firebase-db/companies.service";
import {FirebaseProfileService} from "../../../_shared/services/firebase-db/profile.service";
import {TypeaheadMatch, ModalDirective} from "ngx-bootstrap";
import {ToastsManager} from "ng2-toastr";
import {ProfileDataInterface} from "../../../_shared/interface/profile/profile_data.interface";


@Component({
  selector: 'gb-manage-profile',
  templateUrl: './manage-profile.component.html',
  styleUrls: ['./manage-profile.component.scss']
})
export class ManageProfileComponent implements OnInit {
  @ViewChild('CompanyProfileModal') public CompanyProfileModal: ModalDirective;

  private profileForm: FormGroup;
  private displayname;
  private selectedAdditionalCategories: Array<any>;
  private parametersObservable: any;

  dateObj: string;
  editedBy: string = 'N/A';

  subAuth: Subscription;

  saving: boolean = false;
  switchvalue: boolean = false;

  Categories$: Observable<CategoryInterface[]>; // Can be a Level 1, 2 or 3 Category [name + Key]
  categoryFeatures$: Observable<any[]>; // Load all features by selected Main Category
  companyList$: Observable<CompanyImportInterface[]>; // Load all Companies [name + key]

  sourcePricing$: Observable<any[]>;
  sourceFeatures$: Observable<any[]>;
  sourceSupport$: Observable<any[]>;
  sourceVideoUrl$: Observable<any[]>;
  sourceUsers$: Observable<any[]>;
  sourceSummary$: Observable<any[]>;

  pricingLevel = 1 // Select Pricing Level USD ICONS

  profileKey;

  public asyncSelected: string;
  public typeaheadLoading: boolean;
  public typeaheadNoResults: boolean;
  public dataSource: Observable<any>;

  companySelected: boolean = false;
  selectedCompanyID: number;
  selectedCompanyName: string;

  mainCategorySelected: boolean = true;
  additonalCategoriesSelected: boolean = true;

  constructor(private auth: FirebaseAuthenticationService,
              private catService: FirebaseCategoryService,
              private companyService: FirebaseCompanyService,
              private fb: FormBuilder,
              private profileService: FirebaseProfileService,
              private route: ActivatedRoute,
              private router: Router,
              private toastService: ToastsManager) {
  }


  ngOnInit() {
    this.parametersObservable = this.route.params
      .subscribe((params: Params) => {
        this.loadPage(params['id'])
      })

  }


  /**
   * Init Data load functions with Profile $key
   * @param id
   */
  loadPage(id) {
    if (id != null) {
      this.profileKey = id
      this.getSourcePricingByID(id);
      this.getSourceFeaturesByID(id);
      this.getSourceSupportByID(id);
      this.getSourceVideoUrlByID(id);
      this.getSourceUsersByID(id);
      this.getSourceSummaryID(id);
      this.buildForm();
      this.fetchUsername();
      this.fetchCategories();
    }
  }


  /**
   *
   */
  getSourcePricingByID(id) {
    this.sourcePricing$ = this.profileService.getSourcePricingByProfileID(id)
      .first()
  }


  /**
   *
   */
  getSourceFeaturesByID(id) {
    this.sourceFeatures$ = this.profileService.getSourceFeaturesByID(id)
      .first()
  }


  /**
   *
   */
  getSourceSupportByID(id) {
    this.sourceSupport$ = this.profileService.getSourceSupportByID(id)
      .first()
  }


  /**
   *
   */
  getSourceVideoUrlByID(id) {
    this.sourceVideoUrl$ = this.profileService.getSourceVideoUrlByID(id)
      .first()
  }


  /**
   *
   */
  getSourceUsersByID(id) {
    this.sourceUsers$ = this.profileService.getSourceUsersByID(id)
      .first()
  }


  getSourceSummaryID(id) {
    this.sourceSummary$ = this.profileService.getSourceSummaryByProfileID(id)
  }


  /**
   *
   */
  buildForm() {
    this.profileForm = this.fb.group({
      productName: ['', [Validators.required]],
      productTag: [''],
      productMainCategory: [[Validators.required]],
      isEayToUse: [false],
      isEasySetup: [false],
      isPopular: [false],
      isAffordable: [false],
      productAdditionalCategory: [''],
      productCompanyName: ['', [Validators.required]],
      productCompanyNameID: [''],
      productCompanyURL: [''],
      productPageTitle: ['', [Validators.required]],
      productURL: ['', [Validators.required]],
      productFreeTrialURL: [''],
      productMetaDescription: ['', [Validators.required]],
      productDescription: ['', [Validators.required]],
      productPricingDetailsText: [''],
      productPricingUrl: [''],
      productIsFreeTrail: [false],
      productIsFreeVersion: [false], //Ends profile row 3 - Pricing
      productPricingLevel: [this.pricingLevel], // Icons
      productFeaturesText: [''],
      productFeaturesCheckBoxes: [''], // Load features from associated category (main?)
      productSupport: [''],
      productPhoneSupport: [false],
      productOnlineSupport: [false],
      productRemoteSupport: [false],
      productIsWindows: [false],
      productIsOS: [false],
      productIsIOS: [false],
      productIsAndroid: [false],
      productIsSaaS: [false],
      productIsLinux: [false],
      productIsCloudBased: [false],
      productIsDesktopBased: [false],
      productVideoURl: [''],
      productKeyWords: [''],
      productInternalNotes: [''],
      productPublish: [false],
      needsFollowUp: [false],
      nextUnPublished: [false]
    })

    this.initDataSource();
    this.fetchCategoryFeatures();
    this.getExistingMetaData(this.profileKey);
    this.toggleSwitch();
    this.setNextUnPublishedValue();
  }


  /**
   * Check for Edited Data in Profile Meta Node
   * Otherwise refer to source DATA
   * @param {number} key
   */
  getExistingMetaData(key: number) {
    this.profileService.getProfileMetaDataByID(key)
      .subscribe(data => {
        if (data.$value !== null) {
          this.updateFormWithExistingData(data)
        } else {
          console.log('No EDITED Data Available, go to Source')
          this.updateFormWithSourceData(key)
        }
      })

  }


  /**
   * Prepopulate Form with Sourcedata
   * @param {number} id
   */
  updateFormWithSourceData(id: number) {
    this.profileService.getSourceProfileByID(id)
      .first()
      .subscribe(profile => {
        this.profileForm.patchValue({
          productName: profile.productName,
          productURL: profile.productURL
        })
        this.getCompanyDataByID(profile.companyID);
        this.getMetaDataByID(profile.$key);
        this.getPriceFreetrialByID(profile.$key);
        this.getSupportCheckboxesByID(profile.$key);
        this.getPlatformCheckboxesByID(profile.$key);
        this.getMainCategoryByID(profile.$key);
        this.getAdditionalCategoriesByID(profile.$key);
      })

  }


  /*** IF DATA IS NOT EDITED POPULATE WITH HARVESTED SOURCE DATA **/

  /**
   * Get Main Category and populate form
   * @param id
   */
  getMainCategoryByID(id) {
    this.profileService.getMainCategoryByID(id)
      .first()
      .subscribe(item => {
          if (item.$value !== null) {
            this.mainCategorySelected = true;
            console.log('main categories: ', item)
            this.profileForm.patchValue({
              productMainCategory: item.mainCategory,
            })
          } else {
            this.mainCategorySelected = false;
            console.log('No Main Category in Data')
          }
        }
      )
  }


  /**
   * Get Main Category and populate form
   * @param id
   */
  getAdditionalCategoriesByID(id) {
    this.profileService.getAdditionalCategoriesByID(id)
      .first()
      .subscribe((item) => {
          console.log(item)
          if (item.$value !== null) {
            this.additonalCategoriesSelected = true
            const list = item.join().split(',');
            this.selectedAdditionalCategories = list;
            console.log('additional categories: ', item, list)
            this.profileForm.patchValue({
              productAdditionalCategory: list
            })
          } else {
            this.additonalCategoriesSelected = false;
            console.log('No Additonal Categories in Data')
          }
        }
      )
  }


  /**
   * Get Company Info and populate form
   * @param id
   */
  getCompanyDataByID(id) {
    this.companyService.getCompanyByID(id)
      .first()
      .subscribe((company: CompanyImportInterface) =>
        this.profileForm.patchValue({
          productCompanyName: company.CompanyName,
          productCompanyNameID: company.$key,
          productCompanyURL: company.CompanyURL
        })
      )
  }

  /**
   * Get Meta Data and Populate form
   * @param id
   */
  getMetaDataByID(id) {
    this.profileService.getSourceMetaDataByProfileID(id)
      .first()
      .subscribe(meta => {
          let description: string;
          if (meta.meta1) {
            description = meta.meta1 , meta.meta2 , meta.meta3 , meta.meta4;
          }
          this.profileForm.patchValue({
            productPageTitle: meta.title,
            productMetaDescription: description
          })
        }
      )
  }


  /**
   * Get Meta Data and Populate form
   * @param id
   */
  getPriceFreetrialByID(id) {
    this.profileService.getSourceFreeTrialProfileID(id)
      .first()
      .subscribe(item => {
        const value = Boolean(item)
        this.profileForm.patchValue({
          productIsFreeTrail: value
        })
      })
  }


  /**
   * Get Meta Data and Populate form
   * @param id
   */
  getSupportCheckboxesByID(id) {
    this.profileService.getSourceSupportByID(id)
      .first()
      .subscribe(item => {
        this.profileForm.patchValue({
          productPhoneSupport: item.phone,
          productOnlineSupport: item.videoTutorials,
          productRemoteSupport: item.liveChat,
        })
      })
  }


  /**
   * Get Meta Data and Populate form
   * @param id
   */
  getPlatformCheckboxesByID(id) {
    this.profileService.getSourcePlatformsByID(id)
      .first()
      .subscribe(item => {
        this.profileForm.patchValue({
          productIsWindows: item.windowsInstalled,
          productIsOS: item.macInstalled,
          productIsIOS: item.iOS,
          productIsAndroid: item.android,
          productIsSaaS: item.saaS,
          productIsLinux: item.linux,
        })
      })
  }


  /** Auto Advance functionality **/

  setNextUnPublishedValue() {
    const nextValue = localStorage.getItem('nextUnpublishedProfile')
    if (nextValue) {
      this.profileForm.patchValue({
        nextUnPublished: true
      })
    }
  }

  setNextUnpublished() {
    const value = localStorage.getItem('nextUnpublishedProfile')
    console.log('Pre: ', value)
    if (value) {
      localStorage.removeItem('nextUnpublishedProfile')
      console.log('Value')

    }
    if (!value) {
      localStorage.setItem('nextUnpublishedProfile', 'true')
      console.log('No Value')
    }
    console.log('After Value', localStorage.getItem('nextUnpublishedProfile'))
  }

  goToNextUnpublishedProfile() {
    this.profileService.getNextUnPublishedProfile()
      .first()
      .flatMap(res => res)
      .subscribe(res => {
        console.log(res);
        const key = res.$key;
        this.router.navigate(['/admin/profiles/manage/', res.$key])
          .then(res => this.loadPage(key))
      })
  }


  /**
   * If EDITED Data exist prepopulate Form with data
   * @param {ProfileDataInterface} MetaData
   */
  updateFormWithExistingData(MetaData: ProfileDataInterface) {
    if (MetaData) {
      const m: ProfileDataInterface = MetaData;
      this.profileForm.patchValue({
        productName: m.productName,
        productTag: m.productTagline,
        productMainCategory: m.productMainCategory,
        isEayToUse: m.isEayToUse,
        isEasySetup: m.isEasySetup,
        isPopular: m.isPopular,
        isAffordable: m.isAffordable,
        productAdditionalCategory: m.productAdditionalCategory,
        productCompanyName: m.productCompanyName,
        productCompanyNameID: m.productCompanyID,
        productCompanyURL: m.productCompanyURL,
        productPageTitle: m.productPageTitle,
        productURL: m.productURL,
        productFreeTrialURL: m.productFreeTrialURL,
        productMetaDescription: m.productMetaDescription,
        productDescription: m.productDescription,
        productPricingDetailsText: m.productPricingDetailsText,
        productPricingUrl: m.productPricingUrl,
        productIsFreeTrail: m.productIsFreeTrail,
        productIsFreeVersion: m.productIsFreeVersion, //Ends profile row 3 - Pricing
        productPricingLevel: m.productPricingLevel, // Icons
        productFeaturesText: m.productFeaturesText,
        productFeaturesCheckBoxes: m.productFeaturesCheckBoxes, // Load features from associated category (main?)
        productSupport: m.productSupport,
        productPhoneSupport: m.productPhoneSupport,
        productOnlineSupport: m.productOnlineSupport,
        productRemoteSupport: m.productRemoteSupport,
        productIsWindows: m.productIsWindows,
        productIsOS: m.productIsOS,
        productIsIOS: m.productIsIOS,
        productIsAndroid: m.productIsAndroid,
        productIsSaaS: m.productIsSaaS,
        productIsLinux: m.productIsLinux,
        productIsCloudBased: m.productIsCloudBased,
        productIsDesktopBased: m.productIsDesktopBased,
        productVideoURl: m.productVideoURl,
        productKeyWords: m.productKeyWords,
        productInternalNotes: m.productInternalNotes,
        productPublish: m.isPublished,
        needsFollowUp: m.needsFollowUp
      })
      this.selectedAdditionalCategories = m.productAdditionalCategory;
      this.pricingLevel = m.productPricingLevel;
      this.switchvalue = m.isPublished;
      this.dateObj = m.updatedDate;
      this.editedBy = m.updatedBy;
      this.asyncSelected = m.productCompanyName;
    }

  }


  /**
   * Get All Categories
   */
  fetchCategories() {
    this.Categories$ = this.catService.getAllCategories()
  }


  /**
   * Manages Multi Select of productAdditionalCategory
   * Populates a local array with the chosen category ID's
   * Max 7 id's
   * @param event
   */
  setSelected(event) {
    if (event.length <= 7) {
      this.selectedAdditionalCategories = event;
    } else {
      this.toastService.error('Maximum additional categories is 7', 'Warning')
    }
  }


  /**
   * Typeahead for Companies,
   * if string has value execute search
   */
  initDataSource() {
    this.dataSource = Observable
      .create((observer: any) => {
        // Runs on every search
        observer.next(this.asyncSelected);
      })
      .mergeMap((token: string) => this.getCompanyData(token))
  }


  /**
   * Get All Companies that match search string
   * @param token
   * @returns {Observable<CompanyImportInterface[]>}
   */
  getCompanyData(token: string): Observable<CompanyImportInterface[]> {
    return this.companyList$ = this.companyService.getCompanyByLetter(token)
  }


  /**
   * Show Icon during search typeahead
   * @param e
   */
  public changeTypeaheadLoading(e: boolean): void {
    this.typeaheadLoading = e;
  }

  /**
   * Show No result if search has noresults
   * @param e
   */
  public changeTypeaheadNoResults(e: boolean): void {
    this.typeaheadNoResults = e;
  }

  /**
   * Typeahead selected Company
   * Set ID and Name
   * @param e
   */
  public typeaheadOnSelect(e: TypeaheadMatch): void {
    const value: CompanyImportInterface = e.item;
    this.profileForm.patchValue({
      productCompanyName: value.CompanyName,
      productCompanyNameID: value.$key,
      productCompanyURL: value.CompanyURL
    })
    this.companySelected = true
  }


  fetchCategoryFeatures() {
    this.profileForm.get('productMainCategory').valueChanges
      .subscribe(key =>
        this.categoryFeatures$ = this.catService.getCategoryMetaFeaturesByID(key)
      )

  }

  /** COMPANY PROFILE MODAL **/

  // toggle Modal and set Data

  openCompanyProfileModal(value) {
    if (value) {
      console.log('Open Company Profile Module')
      this.selectedCompanyID = this.profileForm.get('productCompanyNameID').value
      this.selectedCompanyName = this.profileForm.get('productCompanyName').value
      this.CompanyProfileModal.show();
    }
  }


  /**
   * Close Modal
   * @param value
   */
  closeCompanyProfileModal(value) {
    if (value == false)
      this.CompanyProfileModal.hide();
    this.selectedCompanyID = null;
    this.selectedCompanyName = null;
  }


  /**
   *
   * @param id
   */
  modalRouteToProfile(id) {
    this.closeCompanyProfileModal(false)
    this.router.navigate(['/admin/profiles/manage/', id])
      .then(res => res => this.loadPage(id))
  }


  /**
   * Get CategoryName related to Source Category
   * @param value
   * @returns {any}
   */
  //ToDo: How many CategorySources should we show?
  getSourceCategoryName(value) {
    let name
    this.catService.getCategoryByID(value.category)
      .first()
      .subscribe(cat => {
        name = cat.categoryName;
      })
    return name

  }


  /**
   * Pricing Level function
   * @param value
   */
  togglePricingLevel(value) {
    if (value == 1) {
      this.pricingLevel = 1
    }
    if (value == 2) {
      this.pricingLevel = 2
    }
    if (value == 3) {
      this.pricingLevel = 3
    }
    if (value == 4) {
      this.pricingLevel = 4
    }
    console.log('pricing: ', value, '&', this.pricingLevel)

  }


  /**
   *
   */
  toggleSwitch() {
    this.profileForm.get('productPublish').valueChanges
      .subscribe(value => {
          this.switchvalue = value;
          console.log('Sv: ', value, ' = ', this.switchvalue)
        }
      )

  }


  /**
   *
   */
  goToOverview() {
    this.router.navigate(['/admin/profiles'])
      .then(res => this.subAuth.unsubscribe)
  }


  /**
   *
   */
  goToLive(){
    const url = this.profileForm.get('productName').value
    let check = confirm('Did you have save your changes? Please confirm that you want to proceed.');
    if (check) {

      this.router.navigate(['/profile/',url])
    }
  }


  /**
   *
   * @returns {Subscription}
   */
  deleteProfile() {
    let check = confirm('Are you sure you want to delete this profile?')
    if (check) {
      console.log('deleted');
      const primaryCategory = this.profileForm.get('productMainCategory').value
      console.log('Primary Category')

      let allCategories: Array<any> = [];
      if (this.selectedAdditionalCategories) {
        allCategories = this.selectedAdditionalCategories
      }
      console.log('Additonal Categories: ', allCategories)
      allCategories.push(primaryCategory)
      console.log('All Categories: ', allCategories)

      if (allCategories.length) {
        for (let category of allCategories) {
          this.profileService.removeProfileOnCategoryProfileListByID(category, this.profileKey)
            .subscribe()
        }
      }
      return this.profileService.makeInactiveOnSourceProfileByID(this.profileKey)
        .subscribe(res => {
          this.router.navigate(['/admin/profiles']);
          this.toastService.success(this.profileForm.get('productName').value + " Deleted.", 'Success')
        })
    }
  }


  /**
   * Get Username
   */
  fetchUsername() {
    this.subAuth = this.auth.getCurrentUser()
      .subscribe((user: User) => {
        if (user) {
          this.displayname = user.displayName;
          console.log(this.displayname)
        }
      })

  }


  /**
   *
   */
  updateProfile() {
    console.log('save form')
    const formValue = this.profileForm.value;

    // Set Group Members
    const primaryCategory = formValue.productMainCategory;
    console.log('Primary Category')

    let additionalCategories: Array<any> = [];
    if (this.selectedAdditionalCategories) {
      additionalCategories = this.selectedAdditionalCategories
    }
    console.log('Additonal Categories: ', additionalCategories)
    additionalCategories.push(primaryCategory)
    console.log('All Categories: ', additionalCategories)


    // Update CategoryProfileList with this Profile
    let groups = [];
    for (let category of additionalCategories) {
      groups[category] = this.switchvalue
      console.log('Save: ', groups[category])
      this.profileService.updateCategoryProfileListByID(category, this.profileKey, this.switchvalue)
    }

    console.log('This Profile is a member of the following categories: ', groups)


    // check if Features are set
    let featuresGroup = '';
    const featuresCheck = this.profileForm.get('productFeaturesCheckBoxes').value
    if (featuresCheck) {
      featuresGroup = featuresCheck
    }

    console.log('Features checks are: ', featuresGroup)


    // make appropriated constants ready for push

    const metaData = {
      productName: formValue.productName,
      productTagline: formValue.productTag,
      productCompanyName: formValue.productCompanyName,
      productCompanyID: formValue.productCompanyNameID,
      productCompanyURL: formValue.productCompanyURL,
      productMainCategory: formValue.productMainCategory,
      productAdditionalCategory: this.selectedAdditionalCategories,
      productIsWindows: formValue.productIsWindows,
      productIsOS: formValue.productIsOS,
      productIsIOS: formValue.productIsIOS,
      productIsAndroid: formValue.productIsAndroid,
      productIsSaaS: formValue.productIsSaaS,
      productIsLinux: formValue.productIsLinux,
      productIsCloudBased: formValue.productIsCloudBased,
      productIsDesktopBased: formValue.productIsDesktopBased,
      productURL: formValue.productURL,
      productFreeTrialURL: formValue.productFreeTrialURL,
      productDescription: formValue.productDescription,
      productPricingDetailsText: formValue.productPricingDetailsText,
      productPricingLevel: this.pricingLevel, // Icons
      productPricingUrl: formValue.productPricingUrl,
      productIsFreeTrail: formValue.productIsFreeTrail,
      productIsFreeVersion: formValue.productIsFreeVersion,
      productFeaturesText: formValue.productFeaturesText,
      productFeaturesCheckBoxes: featuresGroup, // Load features from associated category (main?) - Ends profile row 4 - Features
      productSupport: formValue.productSupport,
      productPhoneSupport: formValue.productPhoneSupport,
      productOnlineSupport: formValue.productOnlineSupport,
      productRemoteSupport: formValue.productRemoteSupport,
      productVideoURl: formValue.productVideoURl,
      productPageTitle: formValue.productPageTitle,
      productMetaDescription: formValue.productMetaDescription,
      productKeyWords: formValue.productKeyWords,  // Ends Profile Meta tags and OG - SEO
      isEayToUse: formValue.isEayToUse,
      isEasySetup: formValue.isEasySetup,
      isPopular: formValue.isPopular,
      isAffordable: formValue.isPopular,
      updatedBy: this.displayname,
      updatedDate: Date.now(),
      productInternalNotes: formValue.productInternalNotes,
      needsFollowUp: formValue.needsFollowUp,
      isPublished: this.switchvalue,
      groups: groups

    }

    const profileResultMeta = {
      productName: formValue.productName,
      productTagline: formValue.productTag,
      productCompanyName: formValue.productCompanyName,
      productCompanyURL: formValue.productCompanyURL,
      productURL: formValue.productURL,
      productIsFreeTrail: formValue.productIsFreeTrail,
      productIsFreeVersion: formValue.productIsFreeVersion,
      productPricingLevel: this.pricingLevel,
      productIsWindows: formValue.productIsWindows,
      productIsOS: formValue.productIsOS,
      productIsIOS: formValue.productIsIOS,
      productIsAndroid: formValue.productIsAndroid,
      productIsSaaS: formValue.productIsSaaS,
      productIsLinux: formValue.productIsLinux,
      productIsCloudBased: formValue.productIsCloudBased,
      productIsDesktopBased: formValue.productIsDesktopBased,
      isEayToUse: formValue.isEayToUse,
      isEasySetup: formValue.isEasySetup,
      isPopular: formValue.isPopular,
      isAffordable: formValue.isPopular,
    }


    const companyProductList = {
      productName: formValue.productName,
      productCompanyName: formValue.productCompanyName,
      productMainCategory: formValue.productMainCategory,
      isPublished: this.switchvalue,
    }


    // Set values for keys in function below
    const companyKey = formValue.productCompanyNameID;
    const companyName = formValue.productCompanyName;
    const isPublished = this.switchvalue
    console.log('isPublished')

    console.log('Profile Meta: ', metaData);
    console.log('Result Profile Meta: ', profileResultMeta)


    // Save prepared data constants to Database

    this.profileService.updateProfileMetaData(this.profileKey, metaData)
      .map(res => this.profileService.updateSourceProfile(this.profileKey, isPublished, companyName))
      .map(res => this.profileService.updateProfileResultMetaData(this.profileKey, profileResultMeta))
      .map(res => this.companyService.updateCompanyProfileListByID(companyKey, this.profileKey, companyProductList))
      .subscribe(res => {
          this.toastService.success(formValue.productName + ' saved', 'success');
          const nextCheck = localStorage.getItem('nextUnpublishedProfile')
          if (nextCheck) {
            this.goToNextUnpublishedProfile();
          }
          else {
            this.router.navigate(['admin/profiles/'])
          }
        },
        error => {
          this.toastService.warning(formValue.productName + ' not Saved', 'Warning');
          console.error(error);
        }
      )


  }

  //Don't forget to unsubscribe from the Observable
  ngOnDestroy() {
    if (this.parametersObservable != null) {
      this.parametersObservable.unsubscribe();
    }
  }


}

