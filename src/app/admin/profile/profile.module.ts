import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {RouterModule} from "@angular/router";
import {ManageProfileComponent} from "./profile-manage/manage-profile.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {ImageUploadModule} from "../../_partials/image-upload/image-upload.module";
import {ProfilesComponent} from "./profiles-overview/profiles.component";
import {ProfilesLetterContainerComponent} from "./profiles-overview/profiles-letter-container/profiles-letter-container.component";
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import {CompanyProfileOverviewComponent} from "./company-profile-overview/company-profile-overview.component";
import { ModalModule } from 'ngx-bootstrap/modal';

export const routes = [
  {path: '', component: ProfilesComponent, pathMatch: 'full'},
  {path: 'manage/:id', component: ManageProfileComponent, pathMatch: 'full'}
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ModalModule.forRoot(),
    ImageUploadModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    TypeaheadModule
  ],
  declarations: [ProfilesComponent, CompanyProfileOverviewComponent, ProfilesLetterContainerComponent,  ManageProfileComponent],
  exports: [ProfilesComponent, CompanyProfileOverviewComponent, ProfilesLetterContainerComponent,  ManageProfileComponent]
})
export class AdminProfileModule {
}
