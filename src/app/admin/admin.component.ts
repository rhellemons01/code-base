import {Component, OnInit} from '@angular/core';
import {FirebaseAuthenticationService} from "../_shared/services/firebase-db/authentication.service";
import {Router} from "@angular/router";
import {PageTitleService} from "../_shared/services/page-title.service";
import {Observable} from "rxjs/Observable";


@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  user$: Observable<any>;
  pageTitle$: Observable<string>

  constructor(private authService: FirebaseAuthenticationService,
              private titleService: PageTitleService,
              private router: Router) {
  }

  ngOnInit() {
    this.loadTitle()
    this.user$ = this.authService.getCurrentUser();
  }

  /**
   *
   */
  loadTitle() {
    this.pageTitle$ = this.titleService.pageTitle$
  }


  /**
   *
   */
  logOut() {
    this.authService.signOutUser()
      .subscribe(success => {
        this.router.navigate(['/']);
        localStorage.clear();
      })
  }


}
