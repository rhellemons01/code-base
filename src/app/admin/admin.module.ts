import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AdminComponent} from './admin.component';
import {AdminRoutingModule} from "./admin.routing";
import {DashboardModule} from "./dashboard/dashboard.module";
import {CompanyProfileModule} from "./company-profile/company-profile.module";
import {AdminCategoriesModule} from "./categories/categories.module";
import {AdminProfileModule} from "./profile/profile.module";
import {BsDropdownModule} from "ngx-bootstrap";


@NgModule({
  imports: [
    AdminProfileModule,
    AdminRoutingModule,
    AdminCategoriesModule,
    BsDropdownModule,
    CommonModule,
    CompanyProfileModule,
    DashboardModule
  ],
  declarations: [AdminComponent]
})
export class AdminModule {
}
