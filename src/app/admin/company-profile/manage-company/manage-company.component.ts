import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {FirebaseAuthenticationService} from "../../../_shared/services/firebase-db/authentication.service";
import {Subscription} from "rxjs/Subscription";
import 'rxjs/add/operator/switchMap';
import {User} from "firebase/app";

import {CompanyImportInterface} from "../../../_shared/interface/company/company.interface";
import {FirebaseCompanyService} from "../../../_shared/services/firebase-db/companies.service";
import {ToastsManager} from "ng2-toastr";

@Component({
  selector: 'gb-manage-company',
  templateUrl: './manage-company.component.html',
  styleUrls: ['./manage-company.component.scss']
})
export class ManageCompanyComponent implements OnInit {

  private companyForm: FormGroup;
  private displayname;

  dateObj: string;
  editedBy: string = 'N/A';

  currentDate = Date.now();


  companyData: CompanyImportInterface;

  subAuth: Subscription;
  saving: boolean = false;


  constructor(private companyService: FirebaseCompanyService,
              private auth: FirebaseAuthenticationService,
              private fb: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              private toastService: ToastsManager) {
  }


  /**
   *
   */
  ngOnInit() {
    this.buildForm();
    this.fetchUsername();
  }


  getCompanybyID() {
    this.route.params
      .switchMap((params: Params) =>
        this.companyService.getCompanyByID(params['id']))
      .subscribe(companyData => {
        this.companyData = companyData;
        console.log(companyData);
        this.updateForm();
      });

  }


  /**
   *
   */
  buildForm() {
    this.companyForm = this.fb.group({
      companyName: [' ', [Validators.required]],
      companyTagLine: [' '],
      companyUrl: [' ', Validators.required],
      companyDescription: [' '],
      companyOtherNames: [' '],
      companyNotes: [' '],
      needsFollowUp: [false]
    });

    this.getCompanybyID()
  }


  updateForm() {
    this.companyForm.patchValue({
      companyName: this.companyData.CompanyName,
      companyTagLine: this.companyData.CompanyTagLine,
      companyUrl: this.companyData.CompanyURL,
      companyDescription: this.companyData.CompanyDescription,
      companyOtherNames: this.companyData.CompanyOtherNames,
      companyNotes: this.companyData.CompanyNotes,
      needsFollowUp: this.companyData.needsFollowUp
    });
    this.dateObj = this.companyData.updatedDate;
    this.editedBy = this.companyData.updatedBy;

  }


  fetchUsername() {
    this.subAuth = this.auth.getCurrentUser()
      .subscribe((user: User) => {
        if (user) {
          this.displayname = user.displayName;
          console.log(this.displayname)
        }
      })
  }


  /**
   *
   */
  goTodashboard() {
    this.router.navigate(['/admin/companies'])
      .then(res => this.subAuth.unsubscribe)
  }


  deleteCompany() {
    let check = confirm('Are you sure you want to delete ' + this.companyData.CompanyName + '?');
    if (check) {
      console.log('deleted', this.companyData.$key);
      this.companyService.deleteCompanyByID(this.companyData.$key)
        .subscribe(res => {
          this.router.navigate(['/admin/companies']);
          this.toastService.success(this.companyData.CompanyName + " Deleted.", 'Success')
        })
    }
  }


  /**
   *
   */
  updateCompany() {

    const formValue = this.companyForm.value;

    // Check for undefined, if so set value

    /** CompanyName **/
    let companyNameCheck = formValue.companyName
    if (!companyNameCheck) {
      companyNameCheck = '';
    }

    /** CompanyTag**/
    let companyTagCheck = formValue.companyTagLine
    if (!companyTagCheck) {
      companyTagCheck = '';
    }

    /** CompanyURL**/
    let companyURLCheck = formValue.companyUrl
    if (!companyURLCheck) {
      companyURLCheck = '';
    }

    /** CompanyDescription**/
    let companyDescriptionCheck = formValue.companyDescription
    if (!companyDescriptionCheck) {
      companyDescriptionCheck = '';
    }

    /** CompanyNotes**/
    let companyNotesCheck = formValue.companyNotes
    if (!companyNotesCheck) {
      companyNotesCheck = '';
    }

    /** CompanyNeedsFollowUp **/
    let companyNeedsFollowUpCheck = formValue.needsFollowUp
    if (!companyNeedsFollowUpCheck) {
      companyNeedsFollowUpCheck = false;
    }

    /** CompanyOtherNames **/
    let companyOtherNamesCheck = formValue.companyOtherNames
    if (!companyOtherNamesCheck) {
      companyOtherNamesCheck = '';
    }


    const DataToSave = {
      CompanyName: companyNameCheck,
      CompanyTagLine: companyTagCheck,
      CompanyURL: companyURLCheck,
      CompanyDescription: companyDescriptionCheck,
      CompanyNotes: companyNotesCheck,
      needsFollowUp: companyNeedsFollowUpCheck,
      CompanyOtherNames: companyOtherNamesCheck,
      updatedBy: this.displayname,
      updatedDate: Date.now(),
    };


    console.log('Check Form', formValue, 'Save: ', DataToSave);

    this.companyService.updateCompanyByID(DataToSave, this.companyData.$key)
      .subscribe(res => {
          this.toastService.success(this.companyData.CompanyName + " updated.", 'Success');
          this.router.navigate(['/admin/companies']);
        },
        error => this.toastService.error(error, 'Warning'))


  }


}
