import {Component, OnInit,} from '@angular/core';
import {PageTitleService} from "../../../_shared/services/page-title.service";
import {FormBuilder, FormGroup} from "@angular/forms";
import {Observable} from "rxjs/Observable";
import {CompanyImportInterface} from "../../../_shared/interface/company/company.interface";
import {FirebaseCompanyService} from "../../../_shared/services/firebase-db/companies.service";
import {TypeaheadMatch} from "ngx-bootstrap";
import {Router} from "@angular/router";

@Component({
  selector: 'app-company-profile',
  templateUrl: './company-profile.component.html',
  styleUrls: ['./company-profile.component.scss'],
})
export class CompanyProfileComponent implements OnInit {

  controllerForm: FormGroup;

  Alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
  selectedLetter: string = 'a';

  isSearch: boolean = false

  public asyncSelected: string;
  public typeaheadLoading: boolean = false;
  public typeaheadNoResults: boolean;
  public dataSource: Observable<any>;
  profileList$: Observable<CompanyImportInterface[]>; // Load all Profiles[name + key]


  constructor(private companyService: FirebaseCompanyService,
              private titleService: PageTitleService,
              private fb: FormBuilder,
              private router: Router) {
  }

  ngOnInit() {
    this.titleService.insertTitle('Company Management')
    this.buildControllerForm();
    this.initDataSource();
  }

  buildControllerForm() {
    this.controllerForm = this.fb.group({
      searchInput: [],
      collectionSelect: ['companies']
    })


  }


  toggleLetter(value) {
    this.selectedLetter = value;
    this.isSearch = false;
    return 'active-alp'
  }

  setInputLetter(value) {
    console.log(value)
  }

  /**
   * Typeahead for Companies,
   * if string has value execute search
   */
  initDataSource() {
    this.dataSource = Observable
      .create((observer: any) => {
        // Runs on every search
        observer.next(this.asyncSelected);
      })
      .mergeMap((token: string) => this.getProfileData(token))
  }


  /**
   * Get All Profiles that match search string
   * @param token
   * @returns {Observable<CompanyImportInterface[]>}
   */
  getProfileData(token: string): Observable<CompanyImportInterface[]> {
    return this.profileList$ = this.companyService.getCompanyByLetter(token).first()
      .map((list: CompanyImportInterface[]) => list.filter(item => item.isActive == undefined));
  }

  /**
   * Show Icon during search typeahead
   * @param e
   */
  public changeTypeaheadLoading(e: boolean): void {
    this.typeaheadLoading = e;
  }

  /**
   * Show No result if search has noresults
   * @param e
   */
  public changeTypeaheadNoResults(e: boolean): void {
    this.typeaheadNoResults = e;
  }

  /**
   * Typeahead selected Company
   * Set ID and Name
   * @param e
   */
  public typeaheadOnSelect(e: TypeaheadMatch): void {
    const key = e.item.$key
    this.router.navigate(['admin/companies/manage/', key])
    this.controllerForm.reset()
  }


}
