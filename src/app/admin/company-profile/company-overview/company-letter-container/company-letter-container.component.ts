import {Component, EventEmitter, Input, OnInit, Output, Renderer2, SimpleChanges} from '@angular/core';

import {Router} from "@angular/router";

import 'rxjs/add/operator/switchMap';

import {FirebaseCompanyService} from "../../../../_shared/services/firebase-db/companies.service";
import {ToastsManager} from "ng2-toastr";
import {Observable} from "rxjs/Observable";
import {CompanyImportInterface} from "../../../../_shared/interface/company/company.interface";

@Component({
  selector: 'gb-company-by-letter',
  templateUrl: './company-letter-container.component.html',
  styleUrls: ['./company-letter-container.component.scss']
})
export class CompanyLetterContainerComponent implements OnInit {
  companies$: Observable<CompanyImportInterface[]>

  @Input() letter: string = 'a';
  @Output() selectedLetter = new EventEmitter;


  constructor(private companyService: FirebaseCompanyService,
              private router: Router,
              private toastService: ToastsManager) {
  }


  /**
   *
   */
  ngOnInit() {
  }


  ngOnChanges(changes: SimpleChanges) {
    const change1 = changes['letter'];
    if (change1) {
      this.loadCompaniesByLetter(change1.currentValue)
    }
  }


  getProducts(value) {

  }


  loadCompaniesByLetter(letter: string) {
    const check = letter
    let inputLetter = letter;
    if (!check) {
      inputLetter = 'a'
    }
    this.companies$ = this.companyService.getCompanyByLetter(inputLetter)
      .map((list:CompanyImportInterface[]) => list.filter(item => item.isActive == undefined))
    this.selectedLetter.emit(inputLetter)

  }




  /**
   *
   */
  goTodashboard() {
    this.router.navigate(['/admin/companies'])

  }


}
