import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CompanyProfileComponent} from './company-overview/company-profile.component';
import {RouterModule} from "@angular/router";
import {ManageCompanyComponent} from "./manage-company/manage-company.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {InfiniteScrollModule} from "ngx-infinite-scroll";
import {CompanyLetterContainerComponent} from "./company-overview/company-letter-container/company-letter-container.component";
import {TypeaheadModule} from "ngx-bootstrap";

export const routes = [
  {path: '', component: CompanyProfileComponent, pathMatch: 'full'},
  {path: 'manage/:id', component: ManageCompanyComponent, pathMatch: 'full'},
  { path: '**', redirectTo:'' }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    InfiniteScrollModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    TypeaheadModule
  ],
  declarations: [CompanyProfileComponent, CompanyLetterContainerComponent, ManageCompanyComponent],
  exports: [CompanyProfileComponent, CompanyLetterContainerComponent, ManageCompanyComponent]
})
export class CompanyProfileModule {
}
