import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

import {Router} from "@angular/router";

import {FirebaseCategoryService} from "../../../../_shared/services/firebase-db/categories.service";
import {CategoryInterface} from "../../../../_shared/interface/category/category.interface";
import {Subscription} from "rxjs/Subscription";

@Component({
  selector: 'gb-category-by-letter',
  templateUrl: './categories-letter-container.component.html',
  styleUrls: ['./categories-letter-container.component.scss']
})
export class CategoryLetterContainerComponent implements OnChanges {

  /** Backup Data for Filter **/
  sourcecategories: CategoryInterface[] = [];

  /** Data for Template **/
  categories: CategoryInterface[] = [];

  catSub : Subscription;


  @Input() letter: string = 'a';
  @Input() sortbylevel: number
  @Input() sortbypublished: number

  @Output() selectedLetter = new EventEmitter;


  constructor(private categoryService: FirebaseCategoryService,
              private router: Router) {
  }


  ngOnChanges(changes: SimpleChanges) {
    const change1 = changes['letter'];
    if (change1) {
      this.loadCategoriesByLetter(change1.currentValue)
    }

    const change2 = changes['sortbylevel'];
    if (change2) {
      this.filterByLevel(change2.currentValue)
    }

    const change3 = changes['sortbypublished'];
    if (change3) {
      this.filterByPublished(change3.currentValue)
    }

  }


  /**
   * Check to set Icon class for Published
   * @param value
   * @returns {any}
   */
  getPublishedClass(value) {
    if (value) {
      return 'fa-check-circle-o font-green'
    }
    if (!value) {
      return 'fa-circle-o font-grey'
    }
  }


  /**
   * Get Categories from Firebase that match Letter
   * Filter locally due to Capitol and LowerCase entries
   * @param letter
   */
  loadCategoriesByLetter(letter: string) {
    const check = letter;
    let inputLetter = letter;
    if (!check) {
      inputLetter = 'a'
    };

  this.catSub =  this.categoryService.getAllCategories()
      .map(list => list.filter(category => category.categoryName.toLowerCase().charAt(0) === inputLetter))
      .subscribe(list => {
        this.sourcecategories = list;
        this.categories = list;
      })

    this.selectedLetter.emit(inputLetter)
  }


  setCategory(parentID) {
    // If parentID = 0 return level 1
    let level = 'N/A';

    if (parentID === 0) {
      return '1'
    } else {

      let parent$: Subscription = this.categoryService.getCategoryByID(parentID)
        .subscribe(category => {
          if (category.parentId === 0) {
            level = '2'
          }
          else {
            level = '3'
          }
        })
      parent$.unsubscribe();
      return level;
    }

  }


  filterByLevel(Level) {
    const source = this.sourcecategories;

    console.log('Start Filter: ', Level)
    if (Level == 0) {
      console.log('All')
    return  this.categories = source;
    }
    if (Level == 1) {
      console.log('1')
      const list = source.filter(list => {
        return list.parentId === 0
      })
      console.log('1', list)
      return this.categories = list;
    }

    if (Level === 2 || 3) {
      console.log('2 or 3', Level)
      const level2and3 = source.filter(list => {
        return list.parentId !== 0
      })
      let level2 = []
      let level3 = []
      for (const cat of level2and3) {
        const parentID = cat.parentId;
        let parent$: Subscription = this.categoryService.getCategoryByID(parentID)
          .first()
          .subscribe(category => {
            if (category.parentId === 0) {
              level2.push(cat)
            }
            else {
              level3.push(cat)
            }
          })
        parent$.unsubscribe();
      }

      if (Level == 2) {
       return this.categories = level2
      }

      if (Level == 3) {
        return this.categories = level3
      }

    }


  }


  filterByPublished(Status) {
    console.log('Start Filter on Publication: ', Status)
    const source = this.sourcecategories;
    if (Status == 0) {
      console.log('All')
      return this.categories = source;
    }
    if (Status == 1) {
      console.log('Published')
      const listT = source.filter(list => {
        return list.published === true
      })
      console.log('True', listT)
      return this.categories = listT;
    }
    if (Status == 2) {
      console.log('Not Published')
      const list = source.filter(list => {
        return list.published === false
      })
      console.log('False', list)
    return this.categories = list;
    }
  }


  ngOnDestroy(){
    this.catSub.unsubscribe();
    this.categories = [];
    this.sourcecategories = [];
  }

}
