import {Component, OnInit} from '@angular/core';
import {PageTitleService} from '../../../_shared/services/page-title.service';
import {FormBuilder, FormGroup} from "@angular/forms";
import {Observable} from "rxjs/Observable";
import {CategoryInterface} from "../../../_shared/interface/category/category.interface";
import {FirebaseCategoryService} from "../../../_shared/services/firebase-db/categories.service";
import {TypeaheadMatch} from "ngx-bootstrap";
import {Router} from "@angular/router";


@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {

  controllerCategoryForm: FormGroup;

  Alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];

  selectedLetter: string = 'a';
  selectedLevel;
  selectedStatus;

  isSearch: boolean = false

  public asyncSelected: string;
  public typeaheadLoading: boolean = false;
  public typeaheadNoResults: boolean;
  public dataSource: Observable<any>;
  categoryList$: Observable<CategoryInterface[]>; // Load all categories[name + key]


  constructor(private categoryService: FirebaseCategoryService,
              private fb: FormBuilder,
              private titleService: PageTitleService,
              private router: Router) {
  }

  ngOnInit() {
    this.titleService.insertTitle('Category Management');
    this.buildControllerForm();
    this.initDataSource();
  }


  buildControllerForm() {
    this.controllerCategoryForm = this.fb.group({
      searchInput: [''],
      levelSelect: ['0'],
      collectionSelect: ['0']
    })

    this.sortByLevel();
    this.sortByPublishedStatus()

  }


  toggleLetter(value) {
    this.selectedLetter = value;
    this.isSearch = false;
    this.controllerCategoryForm.patchValue({
      levelSelect: '0',
      collectionSelect: '0'
    })
    return 'active-alp'
  }


  sortByLevel() {
    this.controllerCategoryForm.get('levelSelect').valueChanges
      .subscribe(value => {
        this.selectedLevel = value;
        console.log('Level click: ', this.selectedLevel, '=', value)
        if (value != 0) {
          this.controllerCategoryForm.patchValue({
            collectionSelect: '0'
          })
        }
      })
  }

  sortByPublishedStatus() {
    this.controllerCategoryForm.get('collectionSelect').valueChanges
      .subscribe(value => {
        this.selectedStatus = value;
        console.log('Publish click: ', this.selectedStatus, '=', value)
        if (value != 0) {
          this.controllerCategoryForm.patchValue({
            levelSelect: '0'
          })
        }
      })
  }


  //
  // searchFunction () {
  //   this.controllerCategoryForm.get('searchInput').valueChanges
  //     .subscribe(term => {
  //       if(term.length >= 3 ) {
  //         this.isSearch = true;
  //         this.selectedLetter = term
  //         this.controllerCategoryForm.patchValue({
  //           searchInput: ''
  //         })
  //       }
  //     })
  //   this.controllerCategoryForm.patchValue({
  //     levelSelect: '0',
  //     collectionSelect: '0'
  //   })
  // }
  //


  /**
   * Search
   * if string has value execute search
   */
  initDataSource() {
    this.dataSource = Observable
      .create((observer: any) => {
        // Runs on every search
        observer.next(this.asyncSelected);
      })
      .mergeMap((token: string) => this.getProfileData(token))
  }


  /**
   * Get All Profiles that match search string
   * @param token
   * @returns {Observable<CompanyImportInterface[]>}
   */
  getProfileData(token: string): Observable<CategoryInterface[]> {
    return this.categoryList$ = this.categoryService.getCategoriesBySearch(token).first()
      .map((list: CategoryInterface[]) => list.filter(item => item.isActive == undefined));
  }

  /**
   * Show Icon during search typeahead
   * @param e
   */
  public changeTypeaheadLoading(e: boolean): void {
    this.typeaheadLoading = e;
  }

  /**
   * Show No result if search has noresults
   * @param e
   */
  public changeTypeaheadNoResults(e: boolean): void {
    this.typeaheadNoResults = e;
  }

  /**
   * Typeahead selected Company
   * Set ID and Name
   * @param e
   */
  public typeaheadOnSelect(e: TypeaheadMatch): void {
    const key = e.item.$key
    this.router.navigate(['admin/categories/manage/', key])
    this.controllerCategoryForm.reset()
  }


  ngOnDestroy() {
    this.selectedStatus = '';
    this.selectedLevel = ''
  }


}
