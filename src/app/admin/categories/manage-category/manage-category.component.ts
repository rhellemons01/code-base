import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {FirebaseAuthenticationService} from "../../../_shared/services/firebase-db/authentication.service";
import {Subscription} from "rxjs/Subscription";
import {User} from "firebase/app";
import {FirebaseCategoryService} from "../../../_shared/services/firebase-db/categories.service";
import {CategoryInterface} from "../../../_shared/interface/category/category.interface";
import {ToastsManager} from "ng2-toastr";
import {CategoryMetaInterface} from "../../../_shared/interface/category/category_meta.interface";


@Component({
  selector: 'gb-manage-category',
  templateUrl: './manage-category.component.html',
  styleUrls: ['./manage-category.component.scss']
})
export class ManageCategoryComponent implements OnInit, OnDestroy {

  dateObj: string;
  editedBy: string = 'N/A';


  private categoryForm: FormGroup;
  private displayname;

  categoryData: CategoryInterface;
  categoryMetaData: CategoryMetaInterface;

  subAuth: Subscription;
  subCatAll: Subscription;


  saving: boolean = false;
  switchvalue: boolean = false;
  isTopCategory: boolean = true;
  loading: boolean = true;

  hideLevel1: boolean = true;
  hideLevel2: boolean = true;


  private categoryList: CategoryInterface[] = [];
  private levelTwo: CategoryInterface[] = [];
  private levelOne: CategoryInterface[] = [];

  featurelist = [];


  constructor(private auth: FirebaseAuthenticationService,
              private categoryService: FirebaseCategoryService,
              private fb: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              private toastService: ToastsManager) {
  }


  /**
   *
   */
  ngOnInit() {
    this.buildForm();
    this.fetchUsername();
  }


  /**
   *
   */
  buildForm() {
    this.categoryForm = this.fb.group({
      categoryName: ['', [Validators.required]],
      categoryType: ['sub'],
      categoryPopular: [false],
      categoryNext: [false],
      levelOneCategory: [],
      levelTwoCategory: [],
      secondaryCategory: [],
      tagsCategory: [''],
      published: [false],
      pageTitle: ['', [Validators.required]],
      categoryUrl: ['', [Validators.required, Validators.pattern('^[a-z0-9-]*$')]],
      categoryHeader: ['', [Validators.required]],
      categorySubHeader: ['', [Validators.required]],
      categoryDescription: ['', [Validators.required]],
      categoryFeatures: ['', [Validators.minLength(3)]],
      categoryNotes: ['']
    })
    this.getCategoryByID();
    this.toggleTopCategory();
    this.togglePublishSwitch();
  }


  /**
   *
   */
  fetchUsername() {
    this.subAuth = this.auth.getCurrentUser()
      .subscribe((user: User) => {
        if (user) {
          this.displayname = user.displayName;
        }
      })
  }


  /**
   *
   */
  loadCategories(key: number) {

    this.subCatAll = this.categoryService.getAllCategories()
      .subscribe(list => {
        this.categoryList = list;
        this.levelOne = list.filter(list =>
          list.parentId === 0)
        // console.log('Level1: ', this.levelOne)
        if (!this.hideLevel2) {
          this.levelTwo =
            list.filter(list =>
              list.parentId === key)
          // console.log('Level2: ', this.levelTwo)
        }

        this.loading = false;
        // console.log('Loading: ', this.loading)
        this.loadCategoryTwoWithValueOne();
      })

  }

  /**
   *
   */
  getCategoryByID() {
    this.route.params
      .switchMap((params: Params) =>
        this.categoryService.getCategoryByID(params['id']))
      .subscribe(categoryData => {
        console.log(categoryData);
        this.categoryData = categoryData;
        this.getCategoryMetaByID(categoryData.$key)
        if (categoryData) {
          this.initCategories(categoryData);
        }
      });

  }


  /**
   *
   */
  togglePublishSwitch() {
    this.categoryForm.get('published').valueChanges
      .subscribe(value => {
        if (value == true) {
          // console.log(true)
          this.switchvalue = true;
        }
        if (value == false) {
          this.switchvalue = false;
        }
      })
  }


  /**
   *
   */
  toggleTopCategory() {
    this.categoryForm.get('categoryType').valueChanges
      .subscribe(value => {
        if (value == 'top') {
          // console.log(true)
          this.isTopCategory = true;
        }
        if (value == 'sub') {
          // console.log(false)
          this.isTopCategory = false;
        }
      })
  }


  /**
   *
   * @param id
   */
  getCategoryMetaByID(id) {
    this.categoryService.getCategoryMetaByID(id)
      .subscribe(res => {
        this.categoryMetaData = res;
        this.updateCategoryForm();
      })
  }


  /**
   *
   */
  loadCategoryTwoWithValueOne() {
    this.categoryForm.get('levelOneCategory').valueChanges
      .subscribe(key => {
        // console.log('L1 changed, get values for Level2 Dropdown')
        if (this.categoryList.length) {
          this.levelTwo = this.categoryList.filter(list => {
            return list.parentId == key
          })
          if (this.levelTwo.length) {
            this.hideLevel2 = false;
          } else {
            this.hideLevel2 = true;
          }
          // console.log('New values for Level2 Dropdown: ', this.levelTwo)
        }
      })
  }


  goToLive(){
    let check = confirm('Did you have save your changes? Please confirm that you want to leave manage ' + this.categoryData.categoryName + '?');
    if (check) {
      this.router.navigate(['/results/', this.categoryData.categoryURL])
    }
  }




  /**
   *
   */
  updateCategoryForm() {
    let popular = false;
    if (this.categoryData.popular === true) {
      popular = true
      console.log('popular')
    }

    let Type = 'sub'
    const check = this.categoryData.parentId;
    if (check === 0) {
      Type = 'top'
    }

    const nextValue = localStorage.getItem('nextUnpublished')
    if (nextValue) {
      this.categoryForm.patchValue({
        categoryNext: true
      })
    }

    this.categoryForm.patchValue({
      categoryName: this.categoryData.categoryName,
      categoryType: Type,
      categoryPopular: popular,
      secondaryCategory: this.categoryData.secondaryCategoryId,
      published: this.categoryData.published,
      tagsCategory: this.categoryMetaData.categoryTags,
      pageTitle: this.categoryMetaData.categoryTitle,
      categoryUrl: this.categoryMetaData.categoryURL,
      categoryHeader: this.categoryMetaData.categoryH1,
      categorySubHeader: this.categoryMetaData.categoryH2,
      categoryDescription: this.categoryMetaData.categoryDescription,
      categoryNotes: this.categoryMetaData.categoryInternalNote,
    })

    this.dateObj = this.categoryData.updatedDate
    this.editedBy = this.categoryData.updatedBy

    if (this.categoryMetaData.features) {
      this.featurelist = this.categoryMetaData.features
    }

  }


  /**
   * Set ID for Dropdowns
   */
  initCategories(category: CategoryInterface) {

    const check = category.parentId;
    console.log('Check for level of Category')
    if (check === 0) {
      this.categoryForm.patchValue({
        categoryType: 'top',
      })
      console.log('Category is a Level1 Category, load levelTwo with key: ', category.$key)
      this.hideLevel1 = true;
      this.hideLevel2 = true;
      this.loadCategories(category.$key)
    } else {
      // If Category is a Level 2
      this.categoryService.getCategoryByID(category.parentId)
        .first()
        .subscribe(res => {
          const check2 = res.parentId;
          if (check2 === 0) {
            this.categoryForm.patchValue({
              levelOneCategory: Number(res.$key)
            })
            this.hideLevel1 = false;
            this.hideLevel2 = true;
            console.log('Category is a Level2 Category, load levelTwo with key: ', res.$key, '=', category.parentId)
            this.loadCategories(res.$key)
          }
          else {
            // If Category is a Level 3:
            // Get Parent of Level 2
            // Set Level2 as ID for dropdown 2
            this.categoryForm.patchValue({
              levelOneCategory: Number(res.parentId),
              levelTwoCategory: Number(res.$key)
            })
            this.hideLevel1 = false;
            this.hideLevel2 = false;
            console.log('Category is a Level3 Category, load levelTwo with key: ', res.parentId, '!=', category.parentId)
            this.loadCategories(res.parentId)
          }
        })
    }

  }


  /**
   *
   */
  deleteCategory() {
    let check = confirm('Are you sure you want to delete ' + this.categoryData.categoryName + '?');
    if (check) {
      console.log('deleted', this.categoryData.$key);
      this.categoryService.deleteCategoryByID(this.categoryData.$key)
        .subscribe(res => {
          this.router.navigate(['/admin/categories']);
          this.toastService.success(this.categoryData.categoryName + " Deleted.", 'Success')
        })
    }
  }


  /**
   *
   */
  addFeature() {
    const value = this.categoryForm.get('categoryFeatures').value
    console.log('Add', value)
    this.featurelist.push({value})
    this.categoryForm.patchValue({'categoryFeatures': ''})
    console.log(this.featurelist)
  }


  /**
   *
   * @param value
   */
  removeFeature(value) {
    console.log(value, this.featurelist)
    this.featurelist = this.featurelist.filter(e => e !== value)

  }


  /**
   *
   */
  goTodashboard() {
    this.router.navigate(['/admin/categories'])
      .then(res => this.subAuth.unsubscribe)
  }


  /**
   *
   */
  saveCategory() {

    const formValue = this.categoryForm.value;

    let setParentID = formValue.levelOneCategory;
    console.log('Set Category ParentID based on Level')

    /** CategorysecondaryCategoryId*/
    let levelOneCategoryCheck = formValue.levelOneCategory;
    if (!levelOneCategoryCheck) {
      console.log('Category is TopLevel, ParentID = undefined', setParentID)
      setParentID = 0;
    }
    else {
      console.log('Category is Level 2 or 3,  ParentID != 0', setParentID)
      const LevelCheck = formValue.levelTwoCategory
      setParentID = formValue.levelOneCategory;
      if (LevelCheck) {
        setParentID = formValue.levelTwoCategory
        console.log('Level 3 Category, ParentID: ', setParentID)
      } else {
        console.log('Level 2 Category, ParentID: ', setParentID)
      }


    }


    /** CategorysecondaryCategoryId*/
    let categorysecondaryCategoryId = formValue.secondaryCategory
    if (!categorysecondaryCategoryId) {
      categorysecondaryCategoryId = 0;
    }


    /** CategoryTag**/
    let categoryTagCheck = formValue.tagsCategory
    if (!categoryTagCheck) {
      categoryTagCheck = '';
    }


    /** CategoryNotes**/
    let categoryNotesCheck = formValue.categoryNotes
    if (!categoryNotesCheck) {
      categoryNotesCheck = '';
    }


    /**
     * Object To Save to '/categories-meta/'
     */
    const saveToMetaCategories = {
      categoryName: formValue.categoryName,
      categoryTags: categoryTagCheck,
      categoryTitle: formValue.pageTitle,
      parentId: setParentID,
      categoryURL: formValue.categoryUrl,
      categoryH1: formValue.categoryHeader,
      categoryH2: formValue.categorySubHeader,
      categoryDescription: formValue.categoryDescription,
      features: this.featurelist,
      categoryInternalNote: categoryNotesCheck,
    }


    /**
     * Object To Save to '/categories/'
     */
    const saveToCategories = {
      categoryName: formValue.categoryName,
      categoryURL: formValue.categoryUrl,
      parentId: setParentID,
      popular: formValue.categoryPopular,
      published: formValue.published,
      secondaryCategoryId: categorysecondaryCategoryId,
      updatedBy: this.displayname,
      updatedDate: Date.now()
    }


    console.log('Old Data: ', this.categoryData)
    console.log('New Data: ', saveToCategories)

    console.log('Old Meta: ', this.categoryMetaData)
    console.log('New Meta: ', saveToMetaCategories)


    /**
     * Set ID and Update Categories in two Nodes
     * '/categories/$id' and '/categories-meta/$id'
     * @type {number}
     */
    const id = this.categoryData.$key
    this.categoryService.updateCategoryByID(id, saveToCategories)
      .flatMap(result => this.categoryService.updateCategoryMetaByID(id, saveToMetaCategories))
      .subscribe(result => {
          this.toastService.success(this.categoryData.categoryName + " updated.", 'Success');
          const nextCheck = localStorage.getItem('nextUnpublished')
          if (nextCheck) {
            this.goToNextUnpublishedCategory();
          } else {
            this.router.navigate(['/admin/categories']);
          }
        },
        error => this.toastService.error(error, 'Warning'))
  }


  /**
   *
   */
  setNextUnpublished() {
    const value = localStorage.getItem('nextUnpublished')
    console.log('Pre: ', value)
    if (value) {
      localStorage.removeItem('nextUnpublished')
      console.log('Value')

    }
    if (!value) {
      localStorage.setItem('nextUnpublished', 'true')
      console.log('No Value')
    }
    console.log('After Value', localStorage.getItem('nextUnpublished'))
  }


  /**
   *
   */
  goToNextUnpublishedCategory() {
    this.categoryService.getNextUnpublishedCategory()
      .first()
      .flatMap(res => res)
      .subscribe(res => {
        console.log(res);
        this.router.navigate(['/admin/categories/manage/', res.$key])
      })
  }


  ngOnDestroy() {
    this.subAuth.unsubscribe();
    this.subCatAll.unsubscribe();
    this.categoryList = [];
    this.levelOne = [];
    this.levelTwo = [];
    this.loading = true;
  }


}
