import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {FirebaseAuthenticationService} from '../../../_shared/services/firebase-db/authentication.service';
import {Subscription} from 'rxjs/Subscription';
import {User} from 'firebase/app';
import {PageTitleService} from '../../../_shared/services/page-title.service';
import {FirebaseCategoryService} from '../../../_shared/services/firebase-db/categories.service';
import {ToastsManager} from 'ng2-toastr';
import {CategoryInterface} from "../../../_shared/interface/category/category.interface";
import {Observable} from "rxjs/Observable";


@Component({
  selector: 'gb-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.scss']
})
export class AddCategoryComponent implements OnInit, OnDestroy {

  private categoryForm: FormGroup;
  private displayname;
  private PublishStatus = 'UnPublished';

  subAuth: Subscription;
  subOne: Subscription;
  subTwo: Subscription;

  saving = false;
  switchvalue = false;
  isTopCategory = true;

  private levelOne$: Observable<CategoryInterface[]>;
  private levelTwo$: Observable<CategoryInterface[]>;

  featurelist = [];


  @Input() selectedCategory;


  constructor(private auth: FirebaseAuthenticationService,
              private catService: FirebaseCategoryService,
              private fb: FormBuilder,
              private route: Router,
              private titleService: PageTitleService,
              private toastService: ToastsManager) {

  }


  /**
   *
   */
  ngOnInit() {
    this.titleService.insertTitle('Add Category')
    this.buildForm();
    this.fetchUsername();
    this.fetchLevelOne();
  }


  /**
   *
   */
  buildForm() {
    this.categoryForm = this.fb.group({
      categoryName: ['', [Validators.required]],
      categoryType: ['top'],
      categoryPopulair: [false],
      levelOneCategory: [''],
      levelTwoCategory: [''],
      secondTopCategory: [''],
      tagsCategory: [''],
      pageTitle: ['', [Validators.required]],
      categoryUrl: ['', [Validators.required, Validators.pattern('^[a-z0-9-]*$')]],
      categoryHeader: ['', [Validators.required]],
      categorySubHeader: ['', [Validators.required]],
      categoryDescription: ['', [Validators.required]],
      categoryFeatures: ['', [Validators.minLength(3)]],
      categoryNotes: ['']
    })

    this.toggleTopCategory();
  }


  /**
   *
   */
  fetchUsername() {
    this.subAuth = this.auth.getCurrentUser()
      .subscribe((user: User) => {
        console.log(user.displayName)
        if (user) {
          this.displayname = user.displayName;
          console.log(this.displayname)
        }
      })

  }


  /**
   *
   */
  fetchLevelOne() {
    this.levelOne$ = this.catService.getLevelOneCategory()

  }


  /**
   *
   */
  fetchLevelTwo(levelOneKey) {
    this.levelTwo$ = this.catService.getLevelTwoCategory(levelOneKey)

  }


  /**
   *
   */
  toggleTopCategory() {
    this.categoryForm.get('categoryType').valueChanges
      .subscribe(value => {
        if (value == 'top') {
          console.log(true)
          this.isTopCategory = true;
        }
        if (value == 'sub') {
          console.log(false)
          this.isTopCategory = false;
        }
      })

    this.categoryForm.get('levelOneCategory').valueChanges
      .subscribe(value => this.fetchLevelTwo(value) )
  }


  /**
   *
   */
  addFeature() {
    const value = this.categoryForm.get('categoryFeatures').value
    console.log('Add', value)
    this.featurelist.push(value)
    this.categoryForm.patchValue({'categoryFeatures': ''})
    console.log(this.featurelist)
  }


  /**
   *
   * @param value
   */
  removeFeature(value) {
    console.log(value, this.featurelist)
    this.featurelist = this.featurelist.filter(e => e !== value)

  }


  /**
   *
   */
  toggleSwitch() {
    this.switchvalue = !this.switchvalue;
    console.log(this.switchvalue)
    if (this.switchvalue == true) {
      this.PublishStatus = 'Published'
    }
    if (this.switchvalue == false) {
      this.PublishStatus = 'UnPublished'
    }

  }


  /**
   *
   */
  goTodashboard() {
    this.route.navigate(['/admin/dashboard'])
      .then(res => this.subAuth.unsubscribe)
  }


  /**
   *
   */
  saveCategory() {

    const formValue = this.categoryForm.value;

    /** Set Current Date **/
    const now = new Date();
    const isoString = now.toISOString();

    /** Set Popular Check **/
    const isPopular = formValue.categoryPopulair;
    console.log('isPopular: ', isPopular)


    /** Set Checks **/
    const level1: string = formValue.levelOneCategory;
    const level2: string = formValue.levelTwoCategory;
    const secondTopCat: string = formValue.secondTopCategory;


    const popularData = {
      categoryName: formValue.categoryName,
      isPublished: this.switchvalue
    }

    /** Set Category Type and populate groups**/
    let type = 2
    let groups;

    /** If Top Category **/
    if (formValue.categoryType == 'top') {
      type = 1
    }


    /** If Sub Category **/
    if (formValue.categoryType == 'sub') {

      console.log('Level 2: ', level2)

      /** Level 2 **/
      type = 2
      groups = {[level1]: false};

      if (secondTopCat.length >= 1) {
        groups = {
          [level1]: false,
          [secondTopCat]: false
        };
      }

      /** Level 3 **/
      if (level2.length >= 1) {
        console.log(formValue.levelTwoCategory)
        type = 3
        groups = {
          [level1]: false,
          [level2]: false
        };

        if (!secondTopCat.match('')) {
          groups = {
            [level1]: false,
            [level2]: false,
            [secondTopCat]: false
          };
        }


      }

    }

    console.log('Type', type)


    /** Populate General Objects **/

    /** Category Object **/
    const catToSave = {
      categoryName: formValue.categoryName,
      categoryLevel: type,
      isPublished: this.switchvalue
    }


    /** Category Data Object **/
    const dataToSave = {
      categoryName: formValue.categoryName,
      categoryLevel: type,
      categoryTitle: formValue.pageTitle,
      categoryURL: formValue.categoryUrl,
      categoryH1: formValue.categoryHeader,
      categoryH2: formValue.categorySubHeader,
      categoryDescription: formValue.categoryDescription,
      features: this.featurelist,
      categoryInternalNote: formValue.categoryNotes,
      createdBy: this.displayname,
      createdDate: isoString,
      isImported: false,
      isPopular: formValue.categoryPopulair,
      isPublished: this.switchvalue,
      updatedBy: this.displayname,
      updatedDate: isoString
    }


    /** LEVEL ONE CATEGORY **/
    //
    // if (type == 1) {
    //
    //   const catLevel1ToSave = {
    //     categoryName: formValue.categoryName,
    //     isPublished: this.switchvalue
    //   }
    //
    //
    //   this.catService.saveLevelOneCategory(catToSave, dataToSave, catLevel1ToSave, popularData)
    //     .subscribe(res => {
    //         this.toastService.success('Level One Saved', 'Success');
    //         this.route.navigateByUrl('/admin/categories')
    //       },
    //       error => {
    //         console.log(error);
    //         this.toastService.error('Can not save', 'Error')
    //       })
    //
    // }
    //
    //
    // /** LEVEL TWO CATEGORY **/
    // if (type == 2) {
    //   const catLevel2ToSave = {
    //     categoryName: formValue.categoryName,
    //     groups: groups,
    //     isPublished: this.switchvalue
    //   }
    //
    //   const level1: string = formValue.levelOneCategory;
    //
    //   this.catService.saveLevelTwoCategory(catToSave, dataToSave, catLevel2ToSave, level1, secondTopCat, popularData)
    //     .subscribe(res => {
    //         this.toastService.success('Level One Saved', 'Success');
    //         this.route.navigateByUrl('/admin/categories')
    //       },
    //       error => {
    //         console.log(error);
    //         this.toastService.error('Can not save', 'Error')
    //       })
    //
    // }
    //
    //
    // /** LEVEL THREE CATEGORY **/
    // if (type == 3) {
    //   const catLevel3ToSave = {
    //     categoryName: formValue.categoryName,
    //     groups: groups,
    //     isPublished: this.switchvalue
    //   }
    //
    //   const level1: string = formValue.levelOneCategory;
    //
    //   this.catService.saveLevelThreeCategory(catToSave, dataToSave, catLevel3ToSave, level1, level2, secondTopCat, popularData)
    //     .subscribe(res => {
    //         this.toastService.success('Level One Saved', 'Success');
    //         this.route.navigateByUrl('/admin/categories')
    //       },
    //       error => {
    //         console.log(error);
    //         this.toastService.error('Can not save', 'Error')
    //       })
    //
    // }


  }


  ngOnDestroy() {
    this.subAuth.unsubscribe();
  }


}
