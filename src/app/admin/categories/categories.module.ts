import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CategoriesComponent } from './category-overview/categories.component';
import {RouterModule} from "@angular/router";
import { ManageCategoryComponent } from './manage-category/manage-category.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {AddCategoryComponent} from "./add-category/add-category.component";
import {CategoryLetterContainerComponent} from "./category-overview/cattegories-letter-container/categories-letter-container.component";
import {TypeaheadModule} from "ngx-bootstrap";

export const routes = [
  {path: '', component: CategoriesComponent, pathMatch: 'full'},
  {path: 'manage/:id', component: ManageCategoryComponent, pathMatch: 'full'},
  { path: '**', redirectTo:'' }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    TypeaheadModule
  ],
  declarations: [CategoriesComponent, ManageCategoryComponent, CategoryLetterContainerComponent,AddCategoryComponent],
  exports:[CategoriesComponent, ManageCategoryComponent, CategoryLetterContainerComponent,AddCategoryComponent]
})
export class AdminCategoriesModule { }
