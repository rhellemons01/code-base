import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule} from "@angular/router";
import {HeaderResultComponent} from "./header-result.component";




@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  declarations: [HeaderResultComponent],
  exports:[HeaderResultComponent]
})
export class HeaderResultModule { }
