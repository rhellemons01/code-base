import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Router} from "@angular/router";


@Component({
  selector: 'gb-header-result',
  templateUrl: './header-result.component.html',
  styleUrls: ['./header-result.component.scss']
})
export class HeaderResultComponent implements OnInit {


  filter : boolean = false
  @Output() showFilter = new EventEmitter;


  constructor(private router : Router) { }

  ngOnInit() {
  }

  clickFilter(){
    this.filter = !this.filter;
    this.showFilter.emit(this.filter);
  }

}
