import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BannerComponent} from "./banner.component";
import {RouterModule} from "@angular/router";

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  declarations: [BannerComponent],
  exports: [BannerComponent]
})
export class BannerModule {
}
