import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Router} from "@angular/router";


@Component({
  selector: 'gb-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  filter : boolean = false
  @Output() showFilter = new EventEmitter;


  constructor(private router : Router) { }

  ngOnInit() {
  }

  clickFilter(){
    this.filter = !this.filter;
    this.showFilter.emit(this.filter);
  }

}
