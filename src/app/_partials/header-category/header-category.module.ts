import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule} from "@angular/router";
import {HeaderCategoryComponent} from "./header-category.component";



@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  declarations: [HeaderCategoryComponent],
  exports:[HeaderCategoryComponent]
})
export class HeaderCategoryModule { }
