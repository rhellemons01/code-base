import {ChangeDetectorRef, Component, EventEmitter, Input, Output, SimpleChanges} from '@angular/core';
import {AngularFireDatabase, FirebaseListObservable} from "angularfire2/database";
import {Observable} from "rxjs/Observable";
import {ProfileImage} from "../../_shared/interface/profile/profile_image.interface";
import {Router} from "@angular/router";

import * as firebase from 'firebase';
import {ToastsManager} from "ng2-toastr";

@Component({
  selector: 'gb-image-upload',
  templateUrl: './image-upload.component.html',
  styleUrls: ['./image-upload.component.scss'],
})
export class ImageUploadComponent {


  storagePath: string;
  databasePath: string;
  imgPreview: boolean = false;
  storage = firebase.storage();


  @Input() profile_key: string;
  @Input() folder: string;


  @Output() image_key = new EventEmitter;


  fileList$: FirebaseListObservable<ProfileImage[]>;
  imageList$: Observable<ProfileImage[]>;

  constructor(public afDB: AngularFireDatabase,
              private toastService: ToastsManager,
              private ref: ChangeDetectorRef,
              public router: Router) {
  }



  ngOnChanges(changes: SimpleChanges) {
    const change1 = changes['profile_key'];
    if (change1) {
      this.setPaths(change1.currentValue)
    }
  }



  /**
   *
   * @param key
   */
  setPaths(key){
    if (key != null) {
      this.storagePath = key + '/' + this.folder + '/';
      this.databasePath = '/images/' + key + '/' + this.folder + '/';
      this.fileList$ = this.afDB.list('images/' + key + '/' + this.folder + '/');
      this.checkForExistingImages(key)
    }
  }

  /**
   *
   * @param key
   */
  checkForExistingImages(key){
    const check = this.fileList$

    check.subscribe(
      list => {
        if (list.length){
          this.imgPreview = true;
          this.loadImages(key)

        }else{
          this.imgPreview = false;
        }
      }
    )

  }

  /**
   * Place items on imageslist
   * @param key
   */
  loadImages(key){
    this.imageList$ = this.fileList$
      .map(itemList =>
        itemList.map(item => {
          let pathReference = this.storage.ref(key + '/' + this.folder + '/' + item.filename);
          let result = {
            $key: item.$key,
            downloadURL: pathReference.getDownloadURL(),
            path: item.path,
            filename: item.filename
          };
          return result;
        })
      );
  }


  /**
   * Upload Image
   * @param event
   */
  upload(event) {
    if (event.target.files && event.target.files[0]) {
      // Create a root reference
      let storageRef = firebase.storage().ref();
      this.imgPreview = false;
      let selectedFile = event.target.files[0]
      console.log(selectedFile);
      // Make local copies of services because "this" will be clobbered
      let af = this.afDB;
      let folder = this.folder;
      let path = `/${this.profile_key}/${this.folder}/${selectedFile.name}`;
      let iRef = storageRef.child(path);
      iRef.put(selectedFile).then((snapshot) => {
        console.log('Uploaded a blob or file! Now storing the reference at', `/${this.profile_key}/${this.folder}/`);
        af.list(this.databasePath).push({path: path, filename: selectedFile.name})
      }).then(res => {
        this.toastService.success('Image uploaded', 'Success')
        this.imgPreview = true;
        this.ref.markForCheck();
        this.loadImage();
        this.image_key.emit(selectedFile.name);

      });
    }

  }


  /**
   *
   */
  loadImage(){
    let storage = firebase.storage();
    this.fileList$ = this.afDB.list('images/' + this.profile_key + '/' + this.folder + '/');
    this.imageList$ = this.fileList$
      .map(itemList =>
        itemList.map(item => {
          if(item != null) {
            let pathReference = storage.ref(this.profile_key + '/' + this.folder + '/' + item.filename);
            let result = {
              $key: item.$key,
              downloadURL: pathReference.getDownloadURL(),
              path: item.path,
              filename: item.filename
            };

            console.log(result);
            return result;
          }
        })
      );
  }


  /**
   *
   * @param {ProfileImage} image
   */
  delete(image: ProfileImage) {
    let storagePath = image.path;
    let referencePath = this.databasePath + `/` + image.$key;

    // Do these as two separate steps so you can still try delete ref if file no longer exists

    // Delete from Storage
    firebase.storage().ref().child(storagePath).delete()
      .then(() => {
          this.afDB.object(referencePath).remove()
            .then(res => {
              this.toastService.success('Image Removed', 'Success')
              this.imgPreview = false;
              this.ref.markForCheck();
            })
        },
        (error) => console.error("Error deleting stored file", storagePath)
      );

    // Delete references


  }




}
