import {Injectable} from '@angular/core';
import {Observable} from "rxjs/Observable";



@Injectable()
export class AlphabetService {


  private alphabet: string[] = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
  // Observable string stream

  alphabet$: Observable<string>;


  constructor() {

  }


  ngOnInit() {
    this.setABCstring();
  }

  setABCstring() {
    return this.alphabet$ = Observable.create((observer) => {
      observer.next(this.alphabet);
    });
  }








}
