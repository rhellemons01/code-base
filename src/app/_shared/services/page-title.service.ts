import { Injectable } from '@angular/core';
import {Subject} from "rxjs/Subject";

@Injectable()
export class PageTitleService {

  // Observable string source
  private pageTitle = new Subject<string>();

  // Observable string stream
  pageTitle$ = this.pageTitle.asObservable();

  // Service message commands
  insertTitle(title: string) {
    this.pageTitle.next(title)
  }




}
