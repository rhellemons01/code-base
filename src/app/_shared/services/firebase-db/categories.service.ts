import {Injectable} from '@angular/core';

import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';
import {AngularFireDatabase} from 'angularfire2/database';

import {CategoryInterface} from "../../interface/category/category.interface";
import {CategoryMetaInterface} from "../../interface/category/category_meta.interface";


@Injectable()

export class FirebaseCategoryService {

  private DB_CATEGORY_PATH = '/categories/';
  private DB_CATEGORY_META_PATH = '/categories-meta/';


  constructor(private afDB: AngularFireDatabase) {

  }


  getAllCategories(): Observable<CategoryInterface[]> {
    return this.afDB.list(this.DB_CATEGORY_PATH, {
      query: {
        orderByChild: 'categoryName'
      }
    })
  }


  getLevelOneCategory(): Observable<CategoryInterface[]> {
    return this.afDB.list(this.DB_CATEGORY_PATH, {
      query: {
        orderByChild: 'parentId',
        equalTo: 0
      }
    })
  }


  getLevelTwoCategory(levelOneKey: number): Observable<CategoryInterface[]> {
    console.log('Get2 A: ', levelOneKey);
    const key = Number(levelOneKey);
    console.log('Get2: ', key)
    return this.afDB.list(this.DB_CATEGORY_PATH, {
      query: {
        orderByChild: 'parentId',
        equalTo: key
      }
    })
  }


  /**
   * Create / Update CategoryMeta Object
   * @param id
   * @param formData
   * @returns {Observable<any>}
   */
  updateCategoryMetaByID(id, formData): Observable<any> {
    return this.fromFirebaseAuthPromise(this.afDB.object(this.DB_CATEGORY_META_PATH + id).update(formData))
  }


  /**
   * Return Companies  where Company match string
   * @param letter
   * @returns {FirebaseListObservable<any[]>}
   */
  getCategoryMetaByID(id): Observable<CategoryMetaInterface> {
    return this.afDB.object(this.DB_CATEGORY_META_PATH + id)
  }


  /**
   * Return Companies  where Company match string
   * @param letter
   * @returns {FirebaseListObservable<any[]>}
   */
  getCategoryMetaFeaturesByID(id): Observable<any[]> {
    return this.afDB.object(this.DB_CATEGORY_META_PATH + id + '/features/')
  }


  /**
   * Create / Update Category Object
   * @param id
   * @param formData
   * @returns {Observable<any>}
   */
  updateCategoryByID(id, formData): Observable<any> {
    return this.fromFirebaseAuthPromise(this.afDB.object(this.DB_CATEGORY_PATH + id).update(formData))
  }


  /**
   * Return Companies  where Company match string
   * @param letter
   * @returns {FirebaseListObservable<any[]>}
   */
  getCategoryByID(id): Observable<CategoryInterface> {
    return this.afDB.object(this.DB_CATEGORY_PATH + id)
  }


  /**
   * Hide Company by adding an isActive field
   * You can query on undefined value for Active Companies and Data is still in database
   * @param id
   * @returns {Observable<any>}
   */
  deleteCategoryByID(id): Observable<any> {
    return this.fromFirebaseAuthPromise(this.afDB.object(this.DB_CATEGORY_PATH + id).update({isActive: false}))
  }


  /**
   * Return Companies  where Company match string
   * @param letter
   * @returns {FirebaseListObservable<any[]>}
   */
  getCategoriesBySearch(letter): Observable<CategoryInterface[]> {
    const starts = letter
    const ends = starts + '\uf8ff'
    console.log(ends)
    return this.afDB.list(this.DB_CATEGORY_PATH, {
      query: {
        orderByChild: 'categoryName',
        startAt: starts,
        endAt: ends
      }
    })
  }


  /**
   * Returns all Published  Categories
   * @returns {FirebaseListObservable<any[]>}
   */
  getCategoryMetaByURL(url): Observable<CategoryMetaInterface[]> {
    return this.afDB.list(this.DB_CATEGORY_META_PATH, {
      query: {
        orderByChild: 'categoryURL',
        equalTo: url,
        limitToFirst: 1
      }
    })
  }


  /**
   * Returns all Published  Categories
   * @returns {FirebaseListObservable<any[]>}
   */
  getPublishedImportedCategories(): Observable<CategoryInterface[]> {
    return this.afDB.list(this.DB_CATEGORY_PATH, {
      query: {
        orderByChild: 'published',
        equalTo: true
      }
    })
  }


  getNextUnpublishedCategory(): Observable<CategoryInterface[]> {
    return this.afDB.list(this.DB_CATEGORY_PATH, {
      query: {
        orderByChild: 'published',
        equalTo: false,
        limitToFirst: 1
      }
    })
  }


  // Convert Promise of Firebase to an Observable
  fromFirebaseAuthPromise(promise): Observable<any> {

    const subject = new Subject<any>();

    promise
      .then(res => {
          subject.next(res);
          subject.complete();
        },
        err => {
          subject.error(err);
          subject.complete();
        }
      );
    return subject.asObservable();
  }

}
