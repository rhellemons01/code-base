import {Injectable} from '@angular/core';

import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';
import {AngularFireDatabase} from 'angularfire2/database';
import {CompanyImportInterface} from "../../interface/company/company.interface";


@Injectable()

export class FirebaseCompanyService {

  // Set Base
  private DB_COMPANIES_PATH = '/company/';
  private DB_COMPANIES_PROFILE_PATH = '/company-profilelist/';

  constructor(private afDB: AngularFireDatabase) {

  }


  /**
   * Return Companies  where Company match string
   * @param letter
   * @returns {FirebaseListObservable<any[]>}
   */
  getCompanyByLetter(letter): Observable<CompanyImportInterface[]> {
    const starts = letter.toLowerCase()
    const ends = starts + '\uf8ff'
    console.log(ends)
    return this.afDB.list(this.DB_COMPANIES_PATH, {
      query: {
        orderByChild: 'CompanyName',
        startAt: starts,
        endAt: ends
      }
    })
  }


  /**
   * Return all Companies
   * @returns {FirebaseListObservable<any[]>}
   */
  getAllCompanies(): Observable<CompanyImportInterface[]> {
    return this.afDB.list(this.DB_COMPANIES_PATH)
  }

  /**
   * Get Company by ID
   * @param id
   * @returns {FirebaseObjectObservable<any>}
   */
  getCompanyByID(id): Observable<CompanyImportInterface> {
    return this.afDB.object(this.DB_COMPANIES_PATH + id)
  }


  /**
   * Update Company by ID
   * @param DataToSave
   * @param id
   * @returns {Observable<any>}
   */
  updateCompanyByID(DataToSave, id): Observable<any> {
    return this.fromFirebaseAuthPromise(this.afDB.object(this.DB_COMPANIES_PATH + id).update(DataToSave))
  }


  /**
   * Hide Company by adding an isActive field
   * You can query on undefined value for Active Companies and Data is still in database
   * @param id
   * @returns {Observable<any>}
   */
  deleteCompanyByID(id): Observable<any> {
    return this.fromFirebaseAuthPromise(this.afDB.object(this.DB_COMPANIES_PATH + id).update({isActive: false}))
  }


  /**
   * Update Companylist with new Profile and data
   * @param {number} companyKey
   * @param {number} profileKey
   * @param dataToSave
   * @returns {Observable<any>}
   */
  updateCompanyProfileListByID(companyKey: number, profileKey: number, dataToSave): Observable<any> {

    return this.fromFirebaseAuthPromise(this.afDB.object(this.DB_COMPANIES_PROFILE_PATH + companyKey + '/'+ profileKey).update(dataToSave))
  }

  /**
   * Remove Profile from Companylist by key
   * @param {number} companyKey
   * @param {number} profileKey
   * @returns {Observable<any>}
   */
  removeCompanyProfileListByID(companyKey: number, profileKey: number): Observable<any> {
    return this.fromFirebaseAuthPromise(this.afDB.object(this.DB_COMPANIES_PROFILE_PATH + companyKey  + '/'+ profileKey).remove())
  }




  // Convert Promise of Firebase to an Observable
  fromFirebaseAuthPromise(promise): Observable<any> {

    const subject = new Subject<any>();

    promise
      .then(res => {
          subject.next(res);
          subject.complete();
        },
        err => {
          subject.error(err);
          subject.complete();
        }
      );
    return subject.asObservable();
  }

}
