import {Injectable} from "@angular/core";

import {AngularFireAuth} from "angularfire2/auth";
import {Observable} from "rxjs/Observable";
import {Subject} from "rxjs/Subject";
import {User} from "firebase/app";


@Injectable()

export class FirebaseAuthenticationService {

  constructor(private afAuth: AngularFireAuth,) {

  }


  /**
   * Sign in with password and email
   * @param email
   * @param password
   * @returns {Observable<any>}
   */
  siginUser(email, password): Observable<any> {
    return this.fromFirebaseAuthPromise(this.afAuth.auth.signInWithEmailAndPassword(email, password))
  }


  /**
   * Register User with password and email
   * @param email
   * @param password
   * @returns {Observable<any>}
   */
  registerUser(email, password): Observable<User> {
    return this.fromFirebaseAuthPromise(this.afAuth.auth.createUserWithEmailAndPassword(email, password))
  }


  signOutUser(): Observable<any> {
    return this.fromFirebaseAuthPromise(this.afAuth.auth.signOut())
  }


  getCurrentUser(): Observable<any> {
    return this.afAuth.authState;
  }


  updateDisplayname(value: string) {
    return  this.fromFirebaseAuthPromise(this.afAuth.auth.currentUser
      .updateProfile({
        displayName: value,
        photoURL: ''
      }))
  }



  resetPassword(email:string) : Observable<any>{
    return this.fromFirebaseAuthPromise(this.afAuth.auth.sendPasswordResetEmail(email))
  }


  // Convert Promise of Firebase to an Observable
  fromFirebaseAuthPromise(promise): Observable<any> {

    const subject = new Subject<any>();

    promise
      .then(res => {
          subject.next(res);
          subject.complete();
        },
        err => {
          subject.error(err);
          subject.complete();
        }
      );
    return subject.asObservable();
  }

}
