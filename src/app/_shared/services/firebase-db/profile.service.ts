import {Injectable} from '@angular/core';

import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';
import {AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable} from 'angularfire2/database';

import { SourceProfileInterface} from "../../interface/profile/source/source-profile.interface";
import {SourceProfileMetaInterface} from "../../interface/profile/source/source-profilemeta.interface";
import {ProfileResultDataInterface} from "../../interface/profile/profile_result.interface";


@Injectable()

export class FirebaseProfileService {

  private DB_SOURCE_PROFILE_PATH = '/source-profile/';
  private DB_CATEGORY_PROFILELIST_PATH = '/category-profilelist/';
  private DB_PROFILERESULT_META_PATH = '/profile-result-meta/';
  private DB_PROFILE_META_PATH = '/profile-meta/';


  constructor(private afDB: AngularFireDatabase) {

  }

  /** Functions to update Mange Profile **/


  /**
   * update CategoryProfilelist node, set Profile as Member to selected category
   * @param {number} categoryKey
   * @param {number} profileKey
   * @param {boolean} isPublished
   * @returns {Observable<any>}
   */
  updateCategoryProfileListByID(categoryKey: number, profileKey: number, isPublished: boolean): Observable<any> {
    const dataToSave = {
      [profileKey]: isPublished
    }
    return this.fromFirebaseAuthPromise(this.afDB.object(this.DB_CATEGORY_PROFILELIST_PATH + categoryKey + '/members/').update(dataToSave))
  }


  /**
   * update Profile Results node with Meta Data for Profile Result
   * @param {number} profileKey
   * @param dataToSave
   * @returns {Observable<any>}
   */

  updateProfileResultMetaData(profileKey: number, dataToSave): Observable<any> {
    return this.fromFirebaseAuthPromise(this.afDB.object(this.DB_PROFILERESULT_META_PATH + profileKey).update(dataToSave))
  }


  /**
   * update Meta Data for Profile details
   * @param {number} profileKey
   * @param dataToSave
   * @returns {Observable<any>}
   */

  updateProfileMetaData(profileKey: number, dataToSave): Observable<any> {
    return this.fromFirebaseAuthPromise(this.afDB.object(this.DB_PROFILE_META_PATH + profileKey).update(dataToSave))
  }



  /**
   * update publish on Source Profile node
   * Source Data is used in the backend to manage publish
   * @param {number} profileKey
   * @param {boolean} isPublished
   * @returns {Observable<any>}
   */
  updateSourceProfile(profileKey: number, isPublished: boolean, companyName : string): Observable<any> {
    const dataToSave = {
      published: isPublished,
      companyName: companyName
    };
    return this.fromFirebaseAuthPromise(this.afDB.object(this.DB_SOURCE_PROFILE_PATH + profileKey).update(dataToSave))
  }



  /** Soft Delete Functions **/


  /**
   * update CategoryProfilelist node, remove Profile as Member to selected category
   * @param {number} categoryKey
   * @param {number} profileKey
   * @returns {Observable<any>}
   */
  removeProfileOnCategoryProfileListByID(categoryKey: number, profileKey: number): Observable<any> {
    return this.fromFirebaseAuthPromise(this.afDB.object(this.DB_CATEGORY_PROFILELIST_PATH + categoryKey + '/members/' + profileKey).remove())
  }



  /**
   * update Source Profile node, remove publish
   * Source Data is used in the backend to manage publish
   * @param {number} profileKey
   * @returns {Observable<any>}
   */
  makeInactiveOnSourceProfileByID(profileKey: number): Observable<any> {
    return this.fromFirebaseAuthPromise(this.afDB.object(this.DB_SOURCE_PROFILE_PATH + profileKey).update({isActive:false}))
  }




  /** Functions to get Profile data from the Database **/



  /**
   * update Meta Data for Profile details
   * @param {number} profileKey
   * @param dataToSave
   * @returns {Observable<any>}
   */

  getProfileMetaDataByID(profileKey: number): Observable<any> {
    return this.afDB.object(this.DB_PROFILE_META_PATH + profileKey)
  }


  /**
   * Filter Profiles on Name and return exact match
   * @param {string} name
   * @returns {FirebaseListObservable<any[]>}
   */
  getProfileMetaDataByName(name: string): Observable<any> {
    return this.afDB.list(this.DB_PROFILE_META_PATH,
      {
        query: {
          orderByChild: 'productName',
          equalTo: name,
          limitToFirst: 1
        }
      })
  }




  /**
   *
   * @returns {FirebaseListObservable<any[]>}
   */
  getAllProfiles(): Observable<SourceProfileInterface[]> {
    return this.afDB.list(this.DB_SOURCE_PROFILE_PATH, {
      query: {
        orderByChild: 'productName'
      }
    })
  }


  /**
   * Return Profiles  where Profiles match string
   * @param letter
   * @returns {FirebaseListObservable<any[]>}
   */
  getProfilesByLetter(letter): Observable<SourceProfileInterface[]> {
    const starts = letter
    const ends = starts + '\uf8ff'
    console.log(ends)
    return this.afDB.list(this.DB_SOURCE_PROFILE_PATH, {
      query: {
        orderByChild: 'productName',
        startAt: starts,
        endAt: ends
      }
    })
  }


  /**
   * Return Profiles  where Profile match string
   * @param letter
   * @returns {FirebaseListObservable<any[]>}
   */
  getProfilesByLetterAndPagination(letter, id): Observable<SourceProfileInterface[]> {
    const key = String(id);
    const starts = letter + ',' + key;
    const ends = letter + '\uf8ff'
    console.log(key, starts)
    return this.afDB.list(this.DB_SOURCE_PROFILE_PATH, {
      query: {
        orderByChild: 'productName',
        startAt: starts,
        endAt: ends,
      }
    })
  }


  /**
   * Return Profiles  where Profile match string
   * @param letter
   * @returns {FirebaseListObservable<any[]>}
   */
  getProfilesByCompany(id): Observable<SourceProfileInterface[]> {
    const key = Number(id);
    return this.afDB.list('/source-profile/', {
      query: {
        orderByChild: 'companyID',
        equalTo: key,
        limitToFirst: 50
      }
    })
  }


  /**
   * Return Profiles  where Profile match string
   * @param letter
   * @returns {FirebaseListObservable<any[]>}
   */
  getProfilesBySearch(letter): Observable<SourceProfileInterface[]> {
    const starts = letter
    const ends = starts + '\uf8ff'
    console.log(ends)
    return this.afDB.list(this.DB_SOURCE_PROFILE_PATH, {
      query: {
        orderByChild: 'productName',
        startAt: starts,
        endAt: ends,
        limitToFirst: 50
      }
    })
  }


  /**
   * Returns all Published  Categories
   * @returns {FirebaseListObservable<any[]>}
   */
  getSourceProfileByID(id): Observable<SourceProfileInterface> {
    return this.afDB.object(this.DB_SOURCE_PROFILE_PATH + id)
  }


  /**
   * Returns all Source Categories related to Source Profile
   * @returns {FirebaseListObservable<any[]>}
   */
  getSourceCategoryByProfileID(id): Observable<any[]> {
    return this.afDB.list('/profile-categories/' + id)
  }


  /**
   *
   * @param id
   * @returns {FirebaseObjectObservable<any>}
   */
  getSourceMetaDataByProfileID(id:number): Observable<SourceProfileMetaInterface> {
    return this.afDB.object('/source-productmetadata/' + id + '/source1/' )
  }


  /**
   * Returns all Source PageTitles related to Source Profile
   * @returns {FirebaseListObservable<any[]>}
   */
  getSourceSummaryByProfileID(id): Observable<any[]> {
    return this.afDB.list('/source-summary/' + id)
  }



  /**
   *
   * @param id
   * @returns {FirebaseObjectObservable<any>}
   */
  getSourceFreeTrialProfileID(id:number): Observable<any> {
    return this.afDB.object('/source-freetrial/' + id + '/source1/' )
  }



  /**
   * Returns all Source PageTitles related to Source Profile
   * @returns {FirebaseListObservable<any[]>}
   */
  getSourcePricingByProfileID(id): Observable<any[]> {
    return this.afDB.list('/source-pricing/' + id)
  }


  /**
   * Returns all Source PageTitles related to Source Profile
   * @returns {FirebaseListObservable<any[]>}
   */
  getSourceFeaturesByID(id): Observable<any[]> {
    return this.afDB.list('/source-features/' + id)
  }



  /**
   * Returns all Source PageTitles related to Source Profile
   * @returns {FirebaseListObservable<any[]>}
   */
  getSourceSupportByID(id): Observable<any> {
    return this.afDB.object('/source-support/' + id + '/source1/')
  }


  /**
   * Returns all Source PageTitles related to Source Profile
   * @returns {FirebaseListObservable<any[]>}
   */
  getSourcePlatformsByID(id): Observable<any> {
    return this.afDB.object('/source-platforms/' + id + '/source1/')
  }



  /**
   * Returns all Source PageTitles related to Source Profile
   * @returns {FirebaseListObservable<any[]>}
   */
  getSourceVideoUrlByID(id): Observable<any[]> {
    return this.afDB.list('/source-videourls/' + id)
  }


  /**
   * Returns all Source PageTitles related to Source Profile
   * @returns {FirebaseListObservable<any[]>}
   */
  getSourceUsersByID(id): Observable<any[]> {
    return this.afDB.list('/source-typicalusers/' + id)
  }


  /**
   * Returns all Source PageTitles related to Source Profile
   * @returns {FirebaseListObservable<any[]>}
   */
  getMainCategoryByID(id): Observable<any> {
    return this.afDB.object('/profile-main-categories/' + id)
  }


  /**
   * Returns all Source PageTitles related to Source Profile
   * @returns {FirebaseListObservable<any[]>}
   */
  getAdditionalCategoriesByID(id): Observable<any> {
    return this.afDB.object('/profile-additonal-categories/' + id)
  }


  /**
   * Returns all Published  Categories
   * @returns {FirebaseListObservable<any[]>}
   */
  getPublishedProfiles(): Observable<SourceProfileInterface[]> {
    return this.afDB.list(this.DB_SOURCE_PROFILE_PATH, {
      query: {
        orderByChild: 'published',
        equalTo: true
      }
    })
  }


  /**
   * Returns next UnPublished Profile
   * @returns {FirebaseListObservable<any[]>}
   */
  getNextUnPublishedProfile(): Observable<SourceProfileInterface[]> {
    return this.afDB.list(this.DB_SOURCE_PROFILE_PATH, {
      query: {
        orderByChild: 'published',
        equalTo: null,
        limitToFirst: 1
      }
    })
  }







  /**
   * Get Profiles per category
   * @param categoryID
   * @returns {FirebaseListObservable<any[]>}
   */
  getProfilesPerCategory(categoryID) : FirebaseListObservable<any[]> {
    return this.afDB.list('/category-profilelist/' + categoryID + '/members/' )
  }








  /**
   * Returns Profile Result By ID
   * @param id
   * @returns {FirebaseObjectObservable<any>}
   */
  getProfileResultByID(id): Observable<ProfileResultDataInterface> {
    return this.afDB.object('/profile-result-meta/' + id)
  }





  // Convert Promise of Firebase to an Observable
  fromFirebaseAuthPromise(promise): Observable<any> {

    const subject = new Subject<any>();

    promise
      .then(res => {
          subject.next(res);
          subject.complete();
        },
        err => {
          subject.error(err);
          subject.complete();
        }
      );
    return subject.asObservable();
  }

}
