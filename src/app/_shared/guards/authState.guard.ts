import {Injectable} from '@angular/core';
import {Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {Observable} from "rxjs";
import 'rxjs/add/operator/map'
import {AngularFireAuth} from 'angularfire2/auth';
import {isNullOrUndefined} from "util";


@Injectable()
export class AuthStateGuard implements CanActivate {


  constructor(private auth: AngularFireAuth, private router: Router) {
    console.log("Start AuthStateGuard");
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.auth.idToken.map(auth => {
      if (isNullOrUndefined(auth)) {
        this.router.navigate(['/login']);
        return false;
      } else {
        return true;
      }
    });


    // return this.auth.authState.map(auth => {
    //   if (isNullOrUndefined(auth)) {
    //     this.router.navigate(['/login']);
    //     return false;
    //   } else {
    //     return true;
    //   }
    // });
  }



}


