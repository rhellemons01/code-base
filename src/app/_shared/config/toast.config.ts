import {ToastOptions} from "ng2-toastr";


export class CustomToastConfigOption extends ToastOptions {
  animate = 'fade'; // you can override any options available
  positionClass = 'toast-bottom-right';
  enableHTML = true;
  newestOnTop = true;
  showCloseButton = true;
}

