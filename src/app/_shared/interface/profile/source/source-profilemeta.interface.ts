export interface SourceProfileMetaInterface {
  $key: string,
  meta1: string,
  meta2: string,
  meta3: string,
  meta4: string,
  title:string
}
