export interface SourceProfileInterface {
  $key: number,
  productName: string,
  companyID: number,
  companyName: string,
  productURL: string
  published: boolean,
  isActive:boolean
}


