export interface ProfileImage {
  path: string;
  filename: string;
  downloadURL?: string;
  $key?: string;
}
