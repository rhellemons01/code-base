export interface ProfileInterface {
  $key: string,
  profileName: string,
  profileGroups: string,
  isPopular: boolean,
  isPublished: boolean,
}
