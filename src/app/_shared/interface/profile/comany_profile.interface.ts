export interface CompanyProfileListInterface {
  $key: string,
  productName: string,
  productCompanyName: string,
  productMainCategory: string,
  isPublished: string
}
