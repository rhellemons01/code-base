export interface CompanyImportInterface {
  $key: string,
  CompanyName: string,
  CompanyTagLine: string,
  CompanyURL: string,
  CompanyDescription: string,
  CompanyNotes: string,
  needsFollowUp: boolean,
  CompanyOtherNames: string
  updatedBy: string,
  updatedDate: string,
  isActive: boolean
}
