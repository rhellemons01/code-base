export interface CategoryInterface {
  $key: number,
  categoryName: string,
  categoryURL:string,
  createdBy: string,
  createdDate: string,
  parentId: number,
  popular: boolean,
  published: boolean,
  secondaryCategoryId: string,
  updatedBy: string,
  updatedDate: string,
  isActive:boolean
}
