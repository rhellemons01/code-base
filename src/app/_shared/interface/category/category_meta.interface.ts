export interface CategoryMetaInterface {
  $key: string,
  categoryName: string,
  categoryTags: string,
  categoryTitle: string,
  categoryURL: string,
  categoryH1: string,
  categoryH2: string,
  categoryDescription: string,
  features: [{
    value:string
  }],
  categoryInternalNote: string,
  createdBy: string,
  createdDate: string,
  parentId: number,
}
