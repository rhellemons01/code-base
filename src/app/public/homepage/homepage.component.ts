import {Component, OnDestroy, OnInit} from '@angular/core';
import {Meta, Title} from "@angular/platform-browser";
import {FirebaseCategoryService} from "../../_shared/services/firebase-db/categories.service";
import {Subscription} from "rxjs/Subscription";
import {CategoryInterface} from "../../_shared/interface/category/category.interface";


@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomePageComponent implements OnInit, OnDestroy {


  firstCol: CategoryInterface[] = [];
  secondCol: CategoryInterface[] = [];
  popularCol: CategoryInterface[] = [];


  subCat: Subscription;


  constructor(private categoryService: FirebaseCategoryService,
              private titleService: Title,
              private metaService: Meta) {
  }

  ngOnInit() {
    this.setSeoService();
    this.loadPublishedCategories()
  }


  /**
   *
   */
  loadPublishedCategories() {
    this.subCat = this.categoryService.getPublishedImportedCategories()
      .subscribe(publishedCategories => {
        if (publishedCategories.length) {
          const sorted = this.filterByAlphabet(publishedCategories)
          this.filterPopularCategory(sorted);
          const list = sorted.filter(list => list.parentId === 0);
          this.firstCol = list.slice(0, (list.length / 2));
          this.secondCol = list.slice((list.length / 2), list.length);

        }
      })

  }

  /**
   *
   * @param {CategoryInterface[]} publishedList
   */
  filterPopularCategory(publishedList: CategoryInterface[]) {
    this.popularCol = publishedList.filter(list => list.popular === true)
  }

  /**
   *
   * @param {CategoryInterface[]} list
   * @returns {CategoryInterface[]}
   */
  filterByAlphabet(list: CategoryInterface[]) {
    return list.sort(function (a, b) {
      if (a.categoryName < b.categoryName) return -1;
      if (a.categoryName > b.categoryName) return 1;
      return 0;
    })
  }

  /**
   *
   */
  setSeoService() {
    this.titleService.setTitle('Small Business Software | Solve Whatever Needs Solving | GrowBaby')

    this.metaService.updateTag({
      content: 'Everyone here at GrowBaby is an entrepreneur and we understand the effort it takes to select and integrate business software into an organization. With entrepreneurship in mind, we have researched and organized the best business software.'
      , name: 'description'
    })
    this.metaService.updateTag({content: "GrowBaby", property: "og:site_name"})
    this.metaService.updateTag({content: "https://www.growbaby.io", property: "og:url"})
    this.metaService.updateTag({content: "I just used GrowBaby to grow my business!", property: "og:title"})
    this.metaService.updateTag({
      content: "Everyone here at GrowBaby is an entrepreneur and we understand the effort it takes to select and integrate business software into an organization. With entrepreneurship in mind, we have researched and organized the best business software."
      , property: "og:description"
    })
    this.metaService.updateTag({content: "https://www.growbaby.io/assets/images/ogimage.png", property: "og:image"})

  }


  ngOnDestroy() {
    this.subCat.unsubscribe();
  }

}
