import {NgModule}             from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomePageComponent} from "./homepage.component";



const homepageRoutes: Routes = [
  {path: '', component: HomePageComponent},
];

@NgModule({
  imports: [
    RouterModule.forChild(homepageRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class HomePageRoutingModule {
}
