import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HomePageComponent} from './homepage.component';
import {HomePageRoutingModule} from "./homepage.routing";
import {FooterModule} from "../../_partials/footer/footer.module";
import {BannerModule} from "../../_partials/banner/banner.module";
import {HeaderHomeModule} from "../../_partials/header-home/header-home.module";

@NgModule({
  imports: [
    CommonModule,
    BannerModule,
    FooterModule,
    HeaderHomeModule,
    HomePageRoutingModule
  ],
  declarations: [HomePageComponent]
})
export class HomePageModule { }
