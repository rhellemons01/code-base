import {Component, Input, OnChanges, SimpleChanges} from '@angular/core';

import {CategoryInterface} from "../../../../_shared/interface/category/category.interface";


@Component({
  selector: 'gb-category-group',
  templateUrl: './category-group.component.html',
  styleUrls: ['./category-group.component.scss']
})
export class CategoryGroupComponent implements OnChanges {

  list: CategoryInterface[] = [];

  @Input() categoryTitle: string;
  @Input() categoryUrl: string;

  @Input() ParentId: string;
  @Input() categoryList: CategoryInterface[];

  constructor() {
  }


  ngOnChanges(changes: SimpleChanges) {
    const change1 = changes['ParentId'];
    if (change1) {
      this.setCategory(change1.currentValue)
    }
  }


  /**
   * Set Category
   * Note: you would display any Level 2 category that does not have a Level 3 associated.
   * However, if the Level 2 category does have one or more Level 3 categories, you would display the Level 3 categories ILO of the Level 2.
   * @param ParentID
   */
  setCategory(ParentID) {
    const parentID = Number(ParentID)
    const level2 = this.categoryList.filter(list => list.parentId === parentID)
    let catList = []
    if (level2.length) {
      level2.forEach(category => {
        const level3 = this.categoryList.filter(list => list.parentId === category.$key)
        if (level3.length) {
          level3.forEach(category => catList.push(category))
        } else {
          catList.push(category)
        }
      })
    }
    this.list = this.filterByAlphabet(catList)


  }


  /**
   *
   * @param {CategoryInterface[]} list
   * @returns {CategoryInterface[]}
   */
  filterByAlphabet(list: CategoryInterface[]) {
    return list.sort(function (a, b) {
      if (a.categoryName < b.categoryName) return -1;
      if (a.categoryName > b.categoryName) return 1;
      return 0;
    })
  }


}
