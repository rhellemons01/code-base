import {Component, Input, OnChanges, OnDestroy, SimpleChanges} from '@angular/core';
import {Subscription} from 'rxjs/Subscription';
import {FirebaseCategoryService} from "../../../_shared/services/firebase-db/categories.service";
import {CategoryInterface} from "../../../_shared/interface/category/category.interface";


@Component({
  selector: 'gb-category-solution',
  templateUrl: './category-solution.component.html',
  styleUrls: ['./category-solution.component.scss']
})
export class CategorySolutionComponent implements OnChanges, OnDestroy {

  categoryList: CategoryInterface[];
  catColRight: CategoryInterface[];
  catColLeft: CategoryInterface[];

  subCategory: Subscription;
  public isCollapsed = false;

  icon = 'glyphicon-menu-up'
  @Input() letter: string;


  constructor(private categoryService: FirebaseCategoryService) {
  }


  ngOnChanges(changes: SimpleChanges) {
    const change1 = changes['letter'];
    if (change1) {
      this.loadCategory(change1.currentValue)
    }
  }

  /**
   *
   * @param letter
   */
  loadCategory(letter) {
    this.subCategory = this.categoryService.getPublishedImportedCategories()
      .subscribe(list => {
        this.categoryList = list;
        const filterList = list.filter(list => list.parentId === 0)
        const sorted = filterList.filter(cat =>
          cat.categoryName.toLowerCase().charAt(0) === letter)
        if (sorted.length) {
          this.isCollapsed = false
          this.icon = 'glyphicon-menu-up';
          this.catColLeft = sorted.slice(0, (sorted.length / 2));
          this.catColRight = sorted.slice((sorted.length / 2), sorted.length);
        }
        if (!sorted.length) {
          this.isCollapsed = true
          this.icon = ''
        }
      })
  }


  /**
   *
   */
  toggleList() {
    this.isCollapsed = !this.isCollapsed

    if (this.isCollapsed == true) {
      this.icon = 'glyphicon-menu-left';
    }

    if (this.isCollapsed == false) {
      this.icon = 'glyphicon-menu-up'
    }
  }

  ngOnDestroy() {
    this.subCategory.unsubscribe();
  }


}
