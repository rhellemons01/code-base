import {Component,  Input, OnChanges, OnDestroy, SimpleChanges} from '@angular/core';
import {Subscription} from 'rxjs/Subscription';
import {FirebaseCategoryService} from "../../../_shared/services/firebase-db/categories.service";
import {CategoryInterface} from "app/_shared/interface/category/category.interface";


@Component({
  selector: 'gb-category-abc',
  templateUrl: './category-abc.component.html',
  styleUrls: ['./category-abc.component.scss']
})
export class CategoryAbcComponent implements OnChanges, OnDestroy {

  catColRight: CategoryInterface[];
  catColLeft: CategoryInterface[];
  subCategory: Subscription;

  public isCollapsed = false;
  icon = 'glyphicon-menu-up'

  @Input() letter: string = 'a';


  constructor(private categoryService: FirebaseCategoryService) {
  }


  ngOnChanges(changes: SimpleChanges) {
    const change1 = changes['letter'];
    if (change1) {
      this.loadCategory(change1.currentValue)
    }
  }

  /**
   *
   * @param letter
   */
  loadCategory(letter) {
    this.subCategory = this.categoryService.getPublishedImportedCategories()
      .map(list => list.filter(cat =>
        cat.categoryName.toLowerCase().charAt(0) === letter))
      .subscribe(list => {
        if (list.length) {
          this.isCollapsed = false
          this.icon = 'glyphicon-menu-up';
          this.catColLeft = list.slice(0, (list.length / 2));
          this.catColRight = list.slice((list.length / 2), list.length);
        }
        if (!list.length) {
          this.isCollapsed = true
          this.icon = ''
        }
      })
  }

  /**
   *
   */
  toggleList() {
    this.isCollapsed = !this.isCollapsed

    if (this.isCollapsed == true) {
      this.icon = 'glyphicon-menu-left';
    }

    if (this.isCollapsed == false) {
      this.icon = 'glyphicon-menu-up'
    }

  }

  ngOnDestroy() {
    this.subCategory.unsubscribe();
  }


}
