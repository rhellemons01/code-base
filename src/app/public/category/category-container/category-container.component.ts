import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from "rxjs/Subscription";
import {FirebaseCategoryService} from "../../../_shared/services/firebase-db/categories.service";

import {Meta, Title} from "@angular/platform-browser";
import {CategoryInterface} from "../../../_shared/interface/category/category.interface";


@Component({
  selector: 'app-category-container',
  templateUrl: './category-container.component.html',
  styleUrls: ['./category-container.component.scss']
})
export class CategoryContainerComponent implements OnInit, OnDestroy {


  Alphabet = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"]
  showAllCategories: boolean = false;

  showSingleCategory: boolean = false
  selectedLetter: string;

  subCategory: Subscription;
  categoryList: CategoryInterface[];

  constructor(private categoryService: FirebaseCategoryService,
              private titleService: Title,
              private metaService: Meta) {
  }

  ngOnInit() {
    this.setSeoService();
    this.loadCategory();
  }

  /**
   *
   */
  loadCategory() {
    this.subCategory = this.categoryService.getPublishedImportedCategories()
      .subscribe(list => this.categoryList = list)
  }

  /**
   *
   * @param value
   */
  togglePresentCategories(value) {
    if (value == true) {
      this.showAllCategories = true;
      this.showSingleCategory = false;
      this.selectedLetter = '';
    }
    if (value == false) {
      this.showAllCategories = false;
      this.showSingleCategory = false;
      this.selectedLetter = '';
    }
  }

  /**
   *
   * @param {string} letter
   * @returns {any}
   */
  activeAlphabetCheck(letter: string) {
    const list = this.categoryList;
    if (list) {
      let check;
      if (this.showAllCategories) {
        check = this.categoryList.filter(cat =>
          cat.categoryName.toLowerCase().charAt(0) === letter)
      }
      if (!this.showAllCategories) {
        const filterList = this.categoryList.filter(cat =>
          cat.parentId === 0)
        check = filterList.filter(cat =>
          cat.categoryName.toLowerCase().charAt(0) === letter)
      }

      if (check.length) {
        return 'active-alp'
      }
      if (!check.length) {
        return 'no-record'
      }
    }

  }

  /**
   *
   */
  setSeoService() {
    this.titleService.setTitle('Browse all business software solutions on GrowBaby')

    this.metaService.updateTag( {
      content: 'Browse our business software directory to compare the best solutions to grow your business.' ,    name: 'description'
    })
    this.metaService.updateTag(  {content: "GrowBaby", property: "og:site_name" })
    this.metaService.updateTag(  {content: "https://www.growbaby.io/categories", property: "og:url"})
    this.metaService.updateTag(  { content: "Browse all business software solutions on GrowBaby | GrowBaby", property: "og:title"})
    this.metaService.updateTag(  {
        content: "Browse our business software directory to compare the best solutions to grow your business.",   property: "og:description"
    })
    this.metaService.updateTag(  {content: "https://www.growbaby.io/assets/images/ogimage.png", property: "og:image"})

  }


  /**
   *
   * @param value
   */
  showCategory(value) {
    this.showSingleCategory = true;
    this.selectedLetter = value;
  }


  ngOnDestroy() {
    this.subCategory.unsubscribe();
  }

}
