import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CategoryContainerComponent } from './category-container/category-container.component';
import { CategoryAbcComponent } from './category-abc/category-abc.component';
import {FooterModule} from "../../_partials/footer/footer.module";
import {HeaderCategoryModule} from "../../_partials/header-category/header-category.module";
import { CollapseModule } from 'ngx-bootstrap/collapse';
import {RouterModule} from "@angular/router";
import {CategorySolutionComponent} from "./category-solution/category-solution.component";
import {CategoryGroupComponent} from "./category-solution/category-group/category-group.component";

@NgModule({
  imports: [
    CommonModule,
    CollapseModule,
    FooterModule,
    HeaderCategoryModule,
    RouterModule
  ],
  declarations: [CategoryContainerComponent, CategoryAbcComponent, CategoryGroupComponent,CategorySolutionComponent],
  exports: [CategoryContainerComponent, CategoryAbcComponent, CategoryGroupComponent,  CategorySolutionComponent]
})
export class CategoryModule { }
