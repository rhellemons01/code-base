import {Component, Inject, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {PrismicService} from "../../../_shared/services/prismic.service";
import {DomSanitizer, Meta} from "@angular/platform-browser";

@Component({
  selector: 'app-documents',
  templateUrl: './documents.component.html',
  styleUrls: ['./documents.component.scss']
})
export class DocumentsComponent implements OnInit {

  @Input() id: string;
  sub: any;
  document: any;
  loaded: boolean = false;

  trustedUrl;

  constructor(private route: ActivatedRoute,
              private prismic: PrismicService,
              private metaService: Meta,
              private sanitizer: DomSanitizer,
              @Inject('LinkResolver') private linkResolver) {
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      const type = params['type'];
      const uid = params['id'];
      console.log(uid, type)
      this.prismic.api()
        .then((api) => api.getByUID(type, uid))
        .then((document) => {
          this.document = document;
          this.loaded = true;
          this.trustedUrl = this.sanitizer.bypassSecurityTrustHtml(document.asHtml(this.linkResolver))
        });
    })
  }


  setMeta() {
    this.metaService.addTags([
      {name: 'twitter:title', content: 'Content Title'},
      {property: 'og:title', content: 'Content Title'}
    ])
  }


}
