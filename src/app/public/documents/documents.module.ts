import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DocumentsComponent } from './document-page/documents.component';
import {DocumentsOverviewComponent} from "./documents-overview/documents-overview.component";

import {RouterModule} from "@angular/router";
import {FooterModule} from "../../_partials/footer/footer.module";
import {HeaderModule} from "../../_partials/header-profile/header.module";

@NgModule({
  imports: [
    CommonModule,
    FooterModule,
    HeaderModule,
    RouterModule
  ],
  declarations: [DocumentsComponent, DocumentsOverviewComponent],
  exports: [DocumentsComponent, DocumentsOverviewComponent]
})
export class DocumentsModule { }
