import {Component, Inject, OnInit} from '@angular/core';
import {PrismicService} from "../../../_shared/services/prismic.service";
import {Http} from "@angular/http";


@Component({
  selector: 'gb-documents-overview',
  templateUrl: './documents-overview.component.html',
  styleUrls: ['./documents-overview.component.scss']
})
export class DocumentsOverviewComponent {

  documents: any[] = [];

  constructor(private prismicService: PrismicService,
              private http: Http,
              @Inject('PrismicEndpoint') private endpoint:string,
              @Inject('LinkResolver') private linkResolver) {

  }



  ngAfterViewInit(){
    const repoEndpoint = this.endpoint.replace("/api", "");
    this.http.post(repoEndpoint + '/app/settings/onboarding/run', {}, new Headers({
      "Content-Type": "application/x-www-form-urlencoded",
      'Access-Control-Allow-Origin': '*'
    })).subscribe(res => null, error => null)



    this.prismicService.api()
      .then((api) => api.query(''))
      .then((response) => {
        console.log(response.results)
        this.documents = response.results;
      });
  }


}
