import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from "@angular/router";
import {FirebaseCategoryService} from "../../../_shared/services/firebase-db/categories.service";
import {Meta, Title} from "@angular/platform-browser";

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import {CategoryMetaInterface} from "../../../_shared/interface/category/category_meta.interface";
import {FirebaseProfileService} from "../../../_shared/services/firebase-db/profile.service";
import {ProfileResultDataInterface} from "../../../_shared/interface/profile/profile_result.interface";
import {FormBuilder, FormGroup} from "@angular/forms";


@Component({
  selector: 'app-profile-page',
  templateUrl: './results-page.component.html',
  styleUrls: ['./results-page.component.scss']
})
export class ResultsPageComponent implements OnInit, OnDestroy {


  controlForm: FormGroup;

  mobileFilter: boolean = false;
  category: CategoryMetaInterface;
  profiles: ProfileResultDataInterface[] = [];
  sourceProfiles: ProfileResultDataInterface[] = [];


  parentBreadCrumb: string;
  parentBreadCrumbUrl: string;
  breadCrumb: string;
  hasParent: boolean = false;

  pricingLevel = 1;

  constructor(private categoryService: FirebaseCategoryService,
              private fb: FormBuilder,
              private profileService: FirebaseProfileService,
              private route: ActivatedRoute,
              private titleService: Title,
              private router: Router,
              private metaService: Meta) {
  }

  ngOnInit() {
    this.getCategoryByUrl();
    this.buildControllerForm();
  }


  buildControllerForm() {
    this.controlForm = this.fb.group({
      desktop: [false],
      cloud: [false],
      pricingLevel: [this.pricingLevel],
      freeVersion: [false],
      freeTrail: [false],
      windows: [false],
      os: [false],
      linux: [false]
    })


  }


  /**
   *  Get Category Data
   */
  getCategoryByUrl() {
    this.route.params
      .switchMap((params: Params) =>
        this.categoryService.getCategoryMetaByURL(params['id']))
      .flatMap(res => res)
      .subscribe(categoryData => {
        this.sourceProfiles = [];
        this.profiles = [];
        this.category = categoryData;
        this.getProfilesOfCategory(categoryData.$key);
        this.setSeoService(categoryData);
        this.setBreadcrumb(categoryData.parentId)
      });

  }


  /**
   *
   * @param id
   */
  getProfilesOfCategory(id) {
    this.profileService.getProfilesPerCategory(id)
      .subscribe(res => {
          res.forEach(item => {
            this.initDataSource(item.$key)
          });
        }
      );

    console.log('profiles', this.profiles)

  }


  /**
   *
   * @param id
   */
  initDataSource(id) {
    console.log(id);
    return this.profileService.getProfileResultByID(id)
      .first()
      .subscribe((item: ProfileResultDataInterface) => {
        this.profiles.push(item);
        this.sourceProfiles.push(item)
        console.log('profiles', this.profiles)
      })


  }

  /** FILTER FUNCTIONS **/
  getDesktopFilter() {
    this.profiles = this.profiles.filter(item => {
      return item.productIsDesktopBased
    })
  }

  getCloudFilter() {
    return this.profiles.filter(item => {
      return item.productIsCloudBased
    }).length
  }

  getFreeVersionFilter() {
    this.profiles = this.profiles.filter(item => {
      return item.productIsFreeVersion
    })
  }


  getFreeTrailFilter() {
    this.profiles = this.profiles.filter(item => {
      return item.productIsFreeTrail
    })
  }

  getPriceLevelFilter() {
    this.profiles = this.profiles.filter(item => {
      return item.productPricingLevel == this.pricingLevel
    })
  }

  getWindowsFilter() {
    this.profiles = this.profiles.filter(item => {
      return item.productIsWindows
    })
  }

  getOsFilter() {
    this.profiles = this.profiles.filter(item => {
      return item.productIsOS
    })
  }

  getLinuxFilter() {
    this.profiles = this.profiles.filter(item => {
      return item.productIsLinux
    })
  }

  resetFilters() {
    this.controlForm.patchValue({
      desktop: false,
      cloud: false,
      pricingLevel: 1,
      freeVersion: false,
      freeTrail: false,
      windows: false,
      os: false,
      linux: false
    })
    this.profiles = this.sourceProfiles;
  }


  /** GET METRICS FOR FILTERS **/

  getDeskTopMetric() {
    return this.profiles.filter(item => {
      return item.productIsDesktopBased
    }).length
  }

  getCloudMetric() {
    return this.profiles.filter(item => {
      return item.productIsCloudBased
    }).length
  }

  getFreeVersionMetric() {
    return this.profiles.filter(item => {
      return item.productIsFreeVersion
    }).length
  }


  getFreeTrailMetric() {
    return this.profiles.filter(item => {
      return item.productIsFreeTrail
    }).length
  }

  getPriceLevelMetric() {
    return this.profiles.filter(item => {
      return item.productPricingLevel == this.pricingLevel
    }).length
  }

  getWindowsMetric() {
    return this.profiles.filter(item => {
      return item.productIsWindows
    }).length
  }

  getOsMetric() {
    return this.profiles.filter(item => {
      return item.productIsOS
    }).length
  }

  getLinuxMetric() {
    return this.profiles.filter(item => {
      return item.productIsLinux
    }).length
  }


  setBreadcrumb(parentId) {
    if (parentId !== 0) {
      this.categoryService.getCategoryByID(parentId)
        .first()
        .subscribe(res => {
          this.hasParent = true;
          this.parentBreadCrumb = res.categoryName;
          this.parentBreadCrumbUrl = res.categoryURL;
          this.breadCrumb = this.category.categoryName;
        })
    } else {
      this.hasParent = false;
      this.breadCrumb = this.category.categoryName
    }

  }


  setSeoService(category: CategoryMetaInterface) {
    this.titleService.setTitle(category.categoryTitle);

    this.metaService.updateTag({
      content: category.categoryDescription, name: 'description'
    });
    this.metaService.updateTag({content: "GrowBaby", property: "og:site_name"});
    this.metaService.updateTag({content: "https://www.growbaby.io/" + category.categoryURL, property: "og:url"});
    this.metaService.updateTag({content: category.categoryTitle, property: "og:title"});
    this.metaService.updateTag({content: category.categoryDescription, property: "og:description"});
    this.metaService.updateTag({content: "https://www.growbaby.io/assets/images/ogimage.png", property: "og:image"})

  }


  toggleMobileFilterFunction(value) {
    this.mobileFilter = value;
    console.log('Filter: ', this.mobileFilter)
  }


  /**
   *
   * @param value
   */
  togglePricingLevel(value) {
    if (value == 1) {
      this.pricingLevel = 1
    }
    if (value == 2) {
      this.pricingLevel = 2
    }
    if (value == 3) {
      this.pricingLevel = 3
    }
    if (value == 4) {
      this.pricingLevel = 4
    }
    this.getPriceLevelMetric()
    this.getPriceLevelFilter()
    console.log('pricing: ', value, '&', this.pricingLevel)

  }


  goToExternalProfilePage(productUrl) {
    let url: string = '';
    if (!/^http[s]?:\/\//.test(productUrl)) {
      url += 'http://';
    }

    url += productUrl;
    window.open(url, '_blank');
  }


  goToProfilePage(productName) {
    this.router.navigate(['/profile/', productName])

  }


  ngOnDestroy() {
    this.profiles = [];
  }

}
