import {Component, Input } from '@angular/core';


@Component({
  selector: 'gb-price-level-block',
  templateUrl: './price-level-block.component.html',
})
export class PriceLevelBlockComponent  {


  @Input() pricingLevel = 1 ;

  constructor() {
  }


}
