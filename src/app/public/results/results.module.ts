import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {FooterModule} from "../../_partials/footer/footer.module";
import {ResultsPageComponent} from "./results-page/results-page.component";
import {HeaderResultModule} from "../../_partials/header-result/header-result.module";
import {ResultsMobilePageComponent} from "./results-mobile-page/results-mobile-page.component";
import {RouterModule} from "@angular/router";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {PriceLevelBlockComponent} from "./priceLevel-block/price-level-block.component";


export const routes = [
  {path: '', component: ResultsPageComponent, pathMatch: 'full'},
  {path: ':id', component: ResultsPageComponent, pathMatch: 'full'},
  { path: '**', redirectTo:'' }
];

@NgModule({
  imports: [
    CommonModule,
    FooterModule,
    HeaderResultModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PriceLevelBlockComponent, ResultsPageComponent, ResultsMobilePageComponent],
  exports: [PriceLevelBlockComponent, ResultsPageComponent, ResultsMobilePageComponent]
})
export class ResultsModule { }
