import {Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges} from '@angular/core';
import {CategoryMetaInterface} from "../../../_shared/interface/category/category_meta.interface";
import {ProfileResultDataInterface} from "../../../_shared/interface/profile/profile_result.interface";
import {FormBuilder, FormGroup} from "@angular/forms";
import {FirebaseCategoryService} from "../../../_shared/services/firebase-db/categories.service";
import {Router} from "@angular/router";

@Component({
  selector: 'gb-results-mobile-page',
  templateUrl: './results-mobile-page.component.html',
  styleUrls: ['./results-mobile-page.component.scss']
})
export class ResultsMobilePageComponent implements OnInit, OnChanges, OnDestroy {

  mobileControlForm: FormGroup;

  @Input() showFilter: boolean = false;
  @Input() category: CategoryMetaInterface;
  @Input() parentID;
  @Input() profiles: ProfileResultDataInterface[] = [];
  @Input() sourceProfiles: ProfileResultDataInterface[] = [];

  parentBreadCrumb: string;
  parentBreadCrumbUrl: string;
  breadCrumb: string;
  hasParent: boolean = false;

  pricingLevel = 1;


  constructor(private fb: FormBuilder,
              private categoryService: FirebaseCategoryService,
              private router: Router,) {
  }

  ngOnInit() {
    this.buildControllerForm()
  }

  ngOnChanges(changes: SimpleChanges) {
    const change1 = changes['category'];
    if (change1) {
      this.setBreadcrumb(change1.currentValue)
    }
  }


  buildControllerForm() {
    this.mobileControlForm = this.fb.group({
      desktop: [false],
      cloud: [false],
      pricingLevel: [this.pricingLevel],
      freeVersion: [false],
      freeTrail: [false],
      windows: [false],
      os: [false],
      linux: [false]
    })


  }


  /** FILTER FUNCTIONS **/
  getDesktopFilter() {
    this.profiles = this.profiles.filter(item => {
      return item.productIsDesktopBased
    })
  }

  getCloudFilter() {
    this.profiles = this.profiles.filter(item => {
      return item.productIsCloudBased
    })
  }

  getFreeVersionFilter() {
    this.profiles = this.profiles.filter(item => {
      return item.productIsFreeVersion
    })
  }


  getFreeTrailFilter() {
    this.profiles = this.profiles.filter(item => {
      return item.productIsFreeTrail
    })
  }

  getPriceLevelFilter() {
    this.profiles = this.profiles.filter(item => {
      return item.productPricingLevel == this.pricingLevel
    })
  }

  getWindowsFilter() {
    this.profiles = this.profiles.filter(item => {
      return item.productIsWindows
    })
  }

  getOsFilter() {
    this.profiles = this.profiles.filter(item => {
      return item.productIsOS
    })
  }

  getLinuxFilter() {
    this.profiles = this.profiles.filter(item => {
      return item.productIsLinux
    })
  }

  resetFilters() {
    this.mobileControlForm.patchValue({
      desktop: false,
      cloud: false,
      pricingLevel: 1,
      freeVersion: false,
      freeTrail: false,
      windows: false,
      os: false,
      linux: false
    })
    this.profiles = this.sourceProfiles;
    this.showFilter = false;
  }


  apply() {
    this.showFilter = false;
  }


  /** GET METRICS FOR FILTERS **/

  getDeskTopMetric() {
    return this.profiles.filter(item => {
      return item.productIsDesktopBased
    }).length
  }

  getCloudMetric() {
    return this.profiles.filter(item => {
      return item.productIsCloudBased
    }).length
  }

  getFreeVersionMetric() {
    return this.profiles.filter(item => {
      return item.productIsFreeVersion
    }).length
  }


  getFreeTrailMetric() {
    return this.profiles.filter(item => {
      return item.productIsFreeTrail
    }).length
  }

  getPriceLevelMetric() {
    return this.profiles.filter(item => {
      return item.productPricingLevel == this.pricingLevel
    }).length
  }

  getWindowsMetric() {
    return this.profiles.filter(item => {
      return item.productIsWindows
    }).length
  }

  getOsMetric() {
    return this.profiles.filter(item => {
      return item.productIsOS
    }).length
  }

  getLinuxMetric() {
    return this.profiles.filter(item => {
      return item.productIsLinux
    }).length
  }


  setBreadcrumb(category) {
    if (category) {
      if (this.category.parentId !== 0) {
        this.categoryService.getCategoryByID(category.parentId)
          .first()
          .subscribe(res => {
            this.hasParent = true;
            this.parentBreadCrumb = res.categoryName;
            this.parentBreadCrumbUrl = res.categoryURL;
            this.breadCrumb = this.category.categoryName;
          })
      } else {
        this.hasParent = false;
        this.breadCrumb = this.category.categoryName
      }
    }
  }


  /**
   *
   * @param value
   */
  togglePricingLevel(value) {
    if (value == 1) {
      this.pricingLevel = 1
    }
    if (value == 2) {
      this.pricingLevel = 2
    }
    if (value == 3) {
      this.pricingLevel = 3
    }
    if (value == 4) {
      this.pricingLevel = 4
    }
    this.getPriceLevelMetric()
    this.getPriceLevelFilter()
    console.log('pricing: ', value, '&', this.pricingLevel)

  }


  goToExternalProfilePage(productUrl) {
    let url: string = '';
    if (!/^http[s]?:\/\//.test(productUrl)) {
      url += 'http://';
    }

    url += productUrl;
    window.open(url, '_blank');
  }


  goToProfilePage(productName) {
    this.router.navigate(['/profile/', productName])

  }


  ngOnDestroy() {
    this.profiles = [];
  }


}
