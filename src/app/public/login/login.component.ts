import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {FirebaseAuthenticationService} from "../../_shared/services/firebase-db/authentication.service";
import {ToastsManager} from "ng2-toastr";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  private loginForm: FormGroup;
  private resetForm: FormGroup;
  isLoading: boolean = false;
  show: boolean = true;

  constructor(private router: Router,
              private authService: FirebaseAuthenticationService,
              private toastr: ToastsManager,
              private fb: FormBuilder) {

  }

  ngOnInit() {
    this.buildForm();
  }

  /**
   * Init Form
   */
  buildForm() {
    this.loginForm = this.fb.group({
      userEmail: ['', [Validators.email, Validators.required]],
      userPassword: ['', [Validators.required]]
    })

    this.resetForm = this.fb.group({
      resetEmail: ['', [Validators.email, Validators.required]],
    })
  }


  /**
   * Login Method
   */
  onLogin() {
    const formValue = this.loginForm.value;
    const email = formValue.userEmail;
    const password = formValue.userPassword;
    this.isLoading = true;
    this.authService.siginUser(email, password)
      .subscribe(succes => this.router.navigate(['admin/dashboard']),
        error => {
          this.isLoading = false;
          console.error(error);
          if (!error.message) {
            this.toastr.error('Server error LOG-001, please contact our helpdesk');
          }
          else {
            this.toastr.error(error.message.toString());
          }

        }
      )
  }


  /**
   *
   */
  toggleReset() {
    this.show = !this.show
  }

  /**
   *
   */
  resetPassword() {
    const email = this.resetForm.get('resetEmail').value
    this.authService.resetPassword(email)
      .subscribe(succes => {
          this.toastr.success('We send an email with instructions, please look in your inbox')
          this.resetForm.reset()
        },
        error => {
          this.isLoading = false;
          console.error(error);
          if (!error.message) {
            this.toastr.error('Server error LOG-002, please contact our helpdesk');
          }
          else {
            this.toastr.error(error.message.toString());
          }

        }
      )
  }


}
