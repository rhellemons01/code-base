import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params} from "@angular/router";
import {FirebaseProfileService} from "../../../_shared/services/firebase-db/profile.service";
import {ProfileDataInterface} from "../../../_shared/interface/profile/profile_data.interface";
import {Meta, Title, DomSanitizer} from "@angular/platform-browser";
import {FirebaseCategoryService} from "../../../_shared/services/firebase-db/categories.service";

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.scss']
})
export class ProfilePageComponent implements OnInit {

  profile: ProfileDataInterface;

  parentBreadCrumb: string;
  parentBreadCrumbUrl: string;
  breadCrumb: string;
  breadCrumbUrl: string;
  activeBreadCrumb: string;
  hasParent: boolean = false;

  pricingLevel: number = 1

  mediaUrl;

  constructor(private categoryService: FirebaseCategoryService,
              private metaService: Meta,
              private profileService: FirebaseProfileService,
              private route: ActivatedRoute,
              private sanitizer: DomSanitizer,
              private titleService: Title) {
  }


  ngOnInit() {
    this.getProfileByUrl()
  }


  /**
   * Get Profile Data
   */
  getProfileByUrl() {
    this.route.params
      .switchMap((params: Params) =>
        this.profileService.getProfileMetaDataByName(params['id']))
      .flatMap(res => res)
      .subscribe((profile: ProfileDataInterface) => {
        console.log(profile);
        this.profile = profile;
        this.setSeoService(profile);
        this.setBreadcrumb(profile);
        this.sanitizeMediaUrl(profile);
        this.setPricingLevel(profile);

      });

  }

  /**
   *  Init USD Icons
   * @param profile
   */
  setPricingLevel(profile) {
    this.pricingLevel = profile.productPricingLevel;
  }


  /**
   *  Init breadcrumb values
   * @param {ProfileDataInterface} profile
   */
  setBreadcrumb(profile: ProfileDataInterface) {
    this.categoryService.getCategoryMetaByID(profile.productMainCategory)
      .subscribe(category => {
        if (category.parentId !== 0) {
          this.categoryService.getCategoryByID(category.parentId)
            .first()
            .subscribe(res => {
              this.hasParent = true;
              this.parentBreadCrumb = res.categoryName;
              this.parentBreadCrumbUrl = res.categoryURL
              this.breadCrumb = category.categoryName;
              this.breadCrumbUrl = category.categoryURL;
              this.activeBreadCrumb = profile.productName;
            })
        } else {
          this.hasParent = false;
          this.breadCrumb = category.categoryName;
          this.activeBreadCrumb = profile.productName;
        }
      })

  }


  /**
   *  By pass CORS security for media url
   * @param {ProfileDataInterface} profile
   */
  sanitizeMediaUrl(profile: ProfileDataInterface) {
    if (profile.productVideoURl) {
      const url: string = profile.productVideoURl;
      console.log(url)
      this.mediaUrl = this.sanitizer.bypassSecurityTrustResourceUrl(url)
    }

  }

  /**
   * Init Seo service to set meta data
   * @param {ProfileDataInterface} profile
   */
  setSeoService(profile: ProfileDataInterface) {
    this.titleService.setTitle(profile.productPageTitle);

    this.metaService.updateTag({
      content: profile.productMetaDescription, name: 'description'
    });
    this.metaService.updateTag({content: "GrowBaby", property: "og:site_name"});
    this.metaService.updateTag({
      content: "https://www.growbaby.io/profile/" + this.profile.productName,
      property: "og:url"
    });
    this.metaService.updateTag({content: profile.productPageTitle, property: "og:title"});
    this.metaService.updateTag({content: profile.productMetaDescription, property: "og:description"});
    this.metaService.updateTag({content: "https://www.growbaby.io/assets/images/ogimage.png", property: "og:image"})
  }


  /**
   * Go to Product Url in a new Tab
   * @param productUrl
   */
  goToExternalProfilePage(productUrl) {
    let url: string = '';
    if (!/^http[s]?:\/\//.test(productUrl)) {
      url += 'http://';
    }

    url += productUrl;
    window.open(url, '_blank');
  }


  /**
   * Go to Free Trail Url in a new Tab
   * @param freetrailUrl
   */
  goToExternalFreeTrailProfilePage(freetrailUrl) {
    let url: string = '';
    if (!/^http[s]?:\/\//.test(freetrailUrl)) {
      url += 'http://';
    }

    url += freetrailUrl;
    window.open(url, '_blank');
  }


}
