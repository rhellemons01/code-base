import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, SimpleChanges} from '@angular/core';
import {AngularFireDatabase, FirebaseListObservable} from "angularfire2/database";
import {Observable} from "rxjs/Observable";
import {ProfileImage} from "../../../_shared/interface/profile/profile_image.interface";
import {Router} from "@angular/router";

import * as firebase from 'firebase';


@Component({
  selector: 'gb-image-container',
  templateUrl: './image-container.component.html',
  styleUrls: ['./image-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProfileImageContainerComponent {

  storagePath: string;
  databasePath: string;
  imgPreview: boolean = false;
  storage = firebase.storage();


  @Input() profile_key: string;
  @Input() folder: string;


  fileList$: FirebaseListObservable<ProfileImage[]>;
  imageList$: Observable<ProfileImage[]>;

  constructor(public afDB: AngularFireDatabase,
              private ref: ChangeDetectorRef,
              public router: Router) {
  }



  ngOnChanges(changes: SimpleChanges) {
    const change1 = changes['profile_key'];
    if (change1) {
      this.setPaths(change1.currentValue)
    }
  }


  /**
   *
   * @param key
   */
  setPaths(key){
    if (key != null) {
      this.storagePath = key + '/' + this.folder + '/';
      this.databasePath = '/images/' + key + '/' + this.folder + '/';
      this.fileList$ = this.afDB.list('images/' + key + '/' + this.folder + '/');
      this.checkForExistingImages(key)
    }
  }


  /**
   *
   * @param key
   */
  checkForExistingImages(key){
    const check = this.fileList$

    check.subscribe(
      list => {
        if (list.length){
          this.imgPreview = true;
          this.loadImages(key)
          this.ref.markForCheck();
        }else{
          this.imgPreview = false;
        }
      }
    )

  }

  /**
   * Place items on imageslist
   * @param key
   */
  loadImages(key){
    this.imageList$ = this.fileList$
      .map(itemList =>
        itemList.map(item => {
          let pathReference = this.storage.ref(key + '/' + this.folder + '/' + item.filename);
          let result = {
            $key: item.$key,
            downloadURL: pathReference.getDownloadURL(),
            path: item.path,
            filename: item.filename
          };
          return result;
        })
      );
  }








}
