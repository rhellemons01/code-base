import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfilePageComponent } from './profile-page/profile-page.component';
import {HeaderModule} from "../../_partials/header-profile/header.module";
import {FooterModule} from "../../_partials/footer/footer.module";
import {ProfileImageContainerComponent} from "./image-container/image-container.component";
import {RouterModule} from "@angular/router";

@NgModule({
  imports: [
    CommonModule,
    FooterModule,
    HeaderModule,
    RouterModule,
  ],
  declarations: [ProfilePageComponent, ProfileImageContainerComponent]
})
export class ProfileModule { }
