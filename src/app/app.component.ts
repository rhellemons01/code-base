import {Component, OnInit, ViewContainerRef} from '@angular/core';
import {ToastsManager} from "ng2-toastr";
import {Router} from "@angular/router";
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-root',
  template: `    
    <router-outlet></router-outlet>
  `
})
export class AppComponent implements OnInit {
  /**
   * APP Constructor
   * @param toastr
   * @param vRef
   */
  constructor(private router: Router,
              private toastr: ToastsManager,
              private titleService: Title,
              vRef: ViewContainerRef) {
    this.toastr.setRootViewContainerRef(vRef);


  }

  ngOnInit() {
    this.setTitle('Small Business Software | Solve Whatever Needs Solving | GrowBaby');
  }

  /**
   *
   * @param {string} newTitle
   */
  public setTitle( newTitle: string) {
    this.titleService.setTitle(  newTitle );
  }




}
