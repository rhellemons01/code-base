import { NgModule }              from '@angular/core';
import { RouterModule, Routes }  from '@angular/router';
import {AuthStateGuard} from "./_shared/guards/authState.guard";
import {DocumentsComponent} from "./public/documents/document-page/documents.component";
import {DocumentsOverviewComponent} from "./public/documents/documents-overview/documents-overview.component";
import {ProfilePageComponent} from "./public/profile/profile-page/profile-page.component";
import {ResultsPageComponent} from "./public/results/results-page/results-page.component";
import {CategoryContainerComponent} from "./public/category/category-container/category-container.component";



const appRoutes: Routes = [
  {
    path: '',
    canActivate: [],
    loadChildren: './public/homepage/homepage.module#HomePageModule'
  },
  {
    path: 'admin',
    canActivate: [AuthStateGuard],
    loadChildren: './admin/admin.module#AdminModule'
  },
  {path: 'categories', component: CategoryContainerComponent},
  {path: 'learn', component: DocumentsOverviewComponent},
  {path: 'profile/:id', component: ProfilePageComponent},
  // {path: ':type/:id', component: DocumentsComponent},
  {
    path: 'login',
    canActivate: [],
    loadChildren: './public/login/login.module#LoginModule'
  },
  {
    path: 'results',
    canActivate: [],
    loadChildren: './public/results/results.module#ResultsModule'
  },
  {
    path: '**', redirectTo: '', pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ],
  providers: []
})

export class AppRoutingModule {
}


